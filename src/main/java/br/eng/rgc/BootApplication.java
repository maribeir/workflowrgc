package br.eng.rgc;

import java.util.Properties;

import javax.faces.application.ProjectStage;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.sql.DataSource;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@ComponentScan("br.eng.rgc")
@EnableJpaRepositories(basePackages="br.eng.rgc.repositories")
public class BootApplication extends SpringBootServletInitializer {
	
	private final String jdbc_url = "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=127.0.0.1)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=XE)))";
	private final String jdbc_username = "workflow_rgc";
	private final String jdbc_password = "workflowrgc";
	private final String jdbc_driverClassName = "oracle.jdbc.OracleDriver";
	private final String hibernate_generate_statistics = "false";
	private final String hibernate_show_sql = "true";
	private final String hibernate_format_sql = "false";
	private final String hibernate_hbm2ddl_auto = "none";
	private final String hibernate_dialect = "org.hibernate.dialect.Oracle10gDialect";
			
    public static void main(String[] args) throws Exception {
        SpringApplication.run(BootApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(BootApplication.class);
    }
    
    @Bean
    public static CustomScopeConfigurer customScopeConfigurer() {
    CustomScopeConfigurer configurer = new CustomScopeConfigurer();
        /*configurer.setScopes(Collections.<String, Object>singletonMap(
                FacesViewScope.NAME, new FacesViewScope()));*/
        return configurer;
    }
  
    @Bean
    public ServletContextInitializer servletContextCustomizer() {
        return new ServletContextInitializer() {
            public void onStartup(ServletContext sc) throws ServletException {
                sc.setAttribute(ProjectStage.PROJECT_STAGE_PARAM_NAME, ProjectStage.Development.name());
            }
        };
    }
    
    @Bean
    public DataSource dataSource() {
    	DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(this.jdbc_driverClassName);
        dataSource.setUrl(this.jdbc_url);
        dataSource.setUsername(this.jdbc_username);
        dataSource.setPassword(this.jdbc_password);
        
        return dataSource;
    }
    
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        
     	HibernateJpaVendorAdapter hibernateJpa = new HibernateJpaVendorAdapter();
     	hibernateJpa.setGenerateDdl(true);
        hibernateJpa.setShowSql(Boolean.parseBoolean(this.hibernate_show_sql));
        hibernateJpa.setDatabasePlatform(this.hibernate_dialect);
        hibernateJpa.setDatabase(Database.ORACLE);

    	Properties properties = new Properties();
    	properties.put("hibernate.dialect", this.hibernate_dialect);
    	properties.put("hibernate.show_sql", this.hibernate_show_sql);
    	properties.put("hibernate.hbm2ddl.auto", this.hibernate_hbm2ddl_auto);
        
        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(dataSource());
        emf.setPackagesToScan("br.eng.rgc.beans");
        emf.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        emf.setJpaVendorAdapter(hibernateJpa);
        emf.setJpaProperties(properties);
        return emf;
    }
    
    @Bean
    public JpaTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }
  
}