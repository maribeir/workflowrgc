package br.eng.rgc.repositories;

import org.springframework.data.repository.CrudRepository;

import br.eng.rgc.beans.DepartamentoBean;

/**
 * Classe de repositório, que herda os recursos de persistência 
 * da classe CrudRepository do Spring, para a tabela Departamento.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 07/2017
 * 
 */
public interface DepartamentoRepository extends CrudRepository<DepartamentoBean, Long>{

}
