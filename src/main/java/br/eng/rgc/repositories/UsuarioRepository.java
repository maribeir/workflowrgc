package br.eng.rgc.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.eng.rgc.beans.UsuarioBean;

/**
 * Classe de repositório, que herda os recursos de persistência 
 * da classe CrudRepository do Spring, para a tabela Usuário.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */

public interface UsuarioRepository extends CrudRepository<UsuarioBean, Long>{

	List<UsuarioBean> findByNome(String nome);
	List<UsuarioBean> findByDisponivel(String disponivel);
	List<UsuarioBean> findByAtivo(String ativo);
	List<UsuarioBean> findByNomeAndDisponivel(String nome, String disponivel);
	List<UsuarioBean> findByNomeAndAtivo(String nome, String ativo);
	List<UsuarioBean> findByNomeAndDisponivelAndAtivo(String nome, String disponivel, String ativo);
	List<UsuarioBean> findByDisponivelAndAtivo(String disponivel, String ativo);
}
