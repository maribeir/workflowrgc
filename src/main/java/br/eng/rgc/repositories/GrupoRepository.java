package br.eng.rgc.repositories;

import org.springframework.data.repository.CrudRepository;

import br.eng.rgc.beans.GrupoBean;

/**
 * Classe de repositório, que herda os recursos de persistência 
 * da classe CrudRepository do Spring, para a tabela Grupo.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */
public interface GrupoRepository extends CrudRepository<GrupoBean, Long>{

}
