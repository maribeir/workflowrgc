package br.eng.rgc.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.eng.rgc.beans.ItemMenuBean;

/**
 * Classe de repositório, que herda os recursos de persistência 
 * da classe CrudRepository do Spring, para a tabela Item Menu.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */
public interface ItemMenuRepository extends CrudRepository<ItemMenuBean, Long>{
	
	@Query("SELECT im FROM ItemMenuBean im, GrupoItemMenuBean gim UsuarioGrupoBean ug " +
            "WHERE ug.grupo.id = gim.grupo.id AND im.itemMenu.id = gim.itemMenu.id " +
            "  AND ug.usuario.username = :username ")
	List<ItemMenuBean> findByUsername(@Param("username") String username);
}
