package br.eng.rgc.beans;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Bean com atributos dos Itens.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */
@Entity
@Table(name = "RGC_ITEM")
public class ItemBean extends BaseBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_ITEM",nullable=false)
	private Long id;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=ClienteBean.class)
	@JoinColumn(name="ID_CLIENTE",nullable=false)
	private ClienteBean cliente;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=DepartamentoBean.class)
	@JoinColumn(name="ID_DEPARTAMENTO",nullable=false)
	private DepartamentoBean departamento;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=NCMBean.class)
	@JoinColumn(name="ID_NCM_PRE_CLASSIFICADO",nullable=true)
	private NCMBean ncmPreClassificado;
	
	@Column(name="S_FABRICANTE",nullable=true)
	private String fabricante;

	@Column(name="S_PART_NUMBER",nullable=false)
	private String partNumber;

	@Column(name="S_PART_NUMBER_RGC",nullable=true)
	private String partNumberRGC;

	@Column(name="S_PART_NUMBER_FABRICANTE",nullable=true)
	private String partNumberFabricante;

	@Column(name="S_DESCRICAO",nullable=false)
	private String descricao;

	@Column(name="D_RECEBIMENTO",nullable=true)
	private Date dataRecebimento;

	@Column(name="D_CADASTRO",nullable=false)
	private Date dataCadastro;

	@Column(name="S_STATUS",nullable=false)
	private String status;
	
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="item")
	private Collection<ClassificacaoBean> classificacoes;
	
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="item")
	private Collection<ItemCatalogoBean> catalogos;

    /**
     *  Construtor vazio.
     */
	public ItemBean(){
		this.id = new Long(0);
		this.cliente = new ClienteBean();
		this.departamento = new DepartamentoBean();
		this.ncmPreClassificado = new NCMBean();
		this.fabricante = "";
		this.partNumber = "";
		this.partNumberRGC = "";
		this.partNumberFabricante = "";
		this.descricao = "";
		this.dataRecebimento = new Date(0);
		this.dataCadastro = new Date(new java.util.Date().getTime());
		this.status = "";
		this.classificacoes = new ArrayList<ClassificacaoBean>();
		this.catalogos = new ArrayList<ItemCatalogoBean>();
	}
    
	/**
	 *  Construtor com parâmetros.
	 *  
	 * @param id Id do Item.
	 * @param cliente Cliente.
	 * @param departamento Departamento.
	 * @param ncmPreClassificado NCM de uma Classificação antiga.
	 * @param fabricante Fabricante do Item.
	 * @param partNumber Part Number do Item.
	 * @param partNumberRGC Part Number do Item gerado pela RGC.
	 * @param partNumberFabricante Part Number do Item informado pelo Fabricante.
	 * @param descricao Descrição do Item.
	 * @param dataRecebimento Data de Recebimento da solicitação de classificação do Item.
	 * @param dataCadastro Data de Cadastro do Item no sistema.
	 * @param status Status do Item.
	 */
	public ItemBean(Long id, ClienteBean cliente, DepartamentoBean departamento, NCMBean ncmPreClassificado, String fabricante,
			        String partNumber, String partNumberRGC, String partNumberFabricante, String descricao, Date dataRecebimento, 
			        Date dataCadastro, String status, Collection<ClassificacaoBean> classificacoes, Collection<ItemCatalogoBean> catalogos) {
		this.id = id;
		this.cliente = cliente;
		this.departamento = departamento;
		this.ncmPreClassificado = ncmPreClassificado;
		this.fabricante = fabricante;
		this.partNumber = partNumber;
		this.partNumberRGC = partNumberRGC;
		this.partNumberFabricante = partNumberFabricante;
		this.descricao = descricao;
		this.dataRecebimento = dataRecebimento;
		this.dataCadastro = dataCadastro;
		this.status = status;
		this.classificacoes = classificacoes;
		this.catalogos = catalogos;
	}

	/**
	 * Recupera Id do Item.
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Define o Id do Item.
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Recupera Cliente.
	 * @return cliente
	 */
	public ClienteBean getCliente() {
		return cliente;
	}
	/**
	 * Define o Cliente.
	 * @param cliente
	 */
	public void setCliente(ClienteBean cliente) {
		this.cliente = cliente;
	}

	/**
	 * Recupera Departamento.
	 * @return departamento
	 */
	public DepartamentoBean getDepartamento() {
		return departamento;
	}
	/**
	 * Define o Departamento.
	 * @param departamento
	 */
	public void setDepartamento(DepartamentoBean departamento) {
		this.departamento = departamento;
	}

	/**
	 * Recupera NCM de uma Classificação antiga.
	 * @return ncmPreClassificado
	 */
	public NCMBean getNcmPreClassificado() {
		return ncmPreClassificado;
	}
	/**
	 * Define o NCM de uma Classificação antiga.
	 * @param ncmPreClassificado
	 */
	public void setNcmPreClassificado(NCMBean ncmPreClassificado) {
		this.ncmPreClassificado = ncmPreClassificado;
	}

	/**
	 * Recupera Fabricante do Item.	
	 * @return fabricante
	 */
	public String getFabricante() {
		return fabricante;
	}
	/**
	 * Define a Fabricante do Item.	
     * @param fabricante
     */
	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	/**
	 * Recupera Part Number do Item.	
	 * @return partNumber
	 */
	public String getPartNumber() {
		return partNumber;
	}
	/**
	 * Define a Part Number do Item.	
     * @param partNumber
     */
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	/**
	 * Recupera Part Number do Item gerado pela RGC.	
	 * @return partNumberRGC
	 */
	public String getPartNumberRGC() {
		return partNumberRGC;
	}
	/**
	 * Define a Part Number do Item gerado pela RGC.	
     * @param partNumberRGC
     */
	public void setPartNumberRGC(String partNumberRGC) {
		this.partNumberRGC = partNumberRGC;
	}

	/**
	 * Recupera Part Number do Item informado pelo Fabricante.	
	 * @return partNumberRGC
	 */
	public String getPartNumberFabricante() {
		return partNumberFabricante;
	}
	/**
	 * Define a Part Number do Item informado pelo Fabricante.	
     * @param partNumberFabricante
     */
	public void setPartNumberFabricante(String partNumberFabricante) {
		this.partNumberFabricante = partNumberFabricante;
	}

	/**
	 * Recupera Descrição do Item.	
	 * @return descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * Define a Descrição do Item.	
     * @param descricao
     */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Recupera Data de Recebimento da solicitação de classificação do Item.	
	 * @return descricao
	 */
	public Date getDataRecebimento() {
		return dataRecebimento;
	}
	/**
	 * Define a Data de Recebimento da solicitação de classificação do Item.	
     * @param dataRecebimento
     */
	public void setDataRecebimento(Date dataRecebimento) {
		this.dataRecebimento = dataRecebimento;
	}

	/**
	 * Recupera Data de Cadastro do Item no sistema.	
	 * @return dataCadastro
	 */
	public Date getDataCadastro() {
		return dataCadastro;
	}
	/**
	 * Define a Data de Cadastro do Item no sistema.	
     * @param dataCadastro
     */
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	/**
	 * Recupera Status do Item.	
	 * @return status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * Define o Status do Item.	
     * @param descricao
     */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Recupera Classificações.	
	 * @return classificacoes
	 */
	public Collection<ClassificacaoBean> getClassificacoes() {
		return classificacoes;
	}
	/**
	 * Define Classificações.	
     * @param classificacoes
     */
	public void setClassificacoes(Collection<ClassificacaoBean> classificacoes) {
		this.classificacoes = classificacoes;
	}

	/**
	 * Recupera Catálogos.	
	 * @return catalogos
	 */
	public Collection<ItemCatalogoBean> getCatalogos() {
		return catalogos;
	}
	/**
	 * Define Catálogos.	
     * @param catalogos
     */
	public void setCatalogos(Collection<ItemCatalogoBean> catalogos) {
		this.catalogos = catalogos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((catalogos == null) ? 0 : catalogos.hashCode());
		result = prime * result
				+ ((classificacoes == null) ? 0 : classificacoes.hashCode());
		result = prime * result + ((cliente == null) ? 0 : cliente.hashCode());
		result = prime * result
				+ ((dataCadastro == null) ? 0 : dataCadastro.hashCode());
		result = prime * result
				+ ((dataRecebimento == null) ? 0 : dataRecebimento.hashCode());
		result = prime * result
				+ ((departamento == null) ? 0 : departamento.hashCode());
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result
				+ ((fabricante == null) ? 0 : fabricante.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((ncmPreClassificado == null) ? 0 : ncmPreClassificado
						.hashCode());
		result = prime * result
				+ ((partNumber == null) ? 0 : partNumber.hashCode());
		result = prime
				* result
				+ ((partNumberFabricante == null) ? 0 : partNumberFabricante
						.hashCode());
		result = prime * result
				+ ((partNumberRGC == null) ? 0 : partNumberRGC.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemBean other = (ItemBean) obj;
		if (catalogos == null) {
			if (other.catalogos != null)
				return false;
		} else if (!catalogos.equals(other.catalogos))
			return false;
		if (classificacoes == null) {
			if (other.classificacoes != null)
				return false;
		} else if (!classificacoes.equals(other.classificacoes))
			return false;
		if (cliente == null) {
			if (other.cliente != null)
				return false;
		} else if (!cliente.equals(other.cliente))
			return false;
		if (dataCadastro == null) {
			if (other.dataCadastro != null)
				return false;
		} else if (!dataCadastro.equals(other.dataCadastro))
			return false;
		if (dataRecebimento == null) {
			if (other.dataRecebimento != null)
				return false;
		} else if (!dataRecebimento.equals(other.dataRecebimento))
			return false;
		if (departamento == null) {
			if (other.departamento != null)
				return false;
		} else if (!departamento.equals(other.departamento))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (fabricante == null) {
			if (other.fabricante != null)
				return false;
		} else if (!fabricante.equals(other.fabricante))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ncmPreClassificado == null) {
			if (other.ncmPreClassificado != null)
				return false;
		} else if (!ncmPreClassificado.equals(other.ncmPreClassificado))
			return false;
		if (partNumber == null) {
			if (other.partNumber != null)
				return false;
		} else if (!partNumber.equals(other.partNumber))
			return false;
		if (partNumberFabricante == null) {
			if (other.partNumberFabricante != null)
				return false;
		} else if (!partNumberFabricante.equals(other.partNumberFabricante))
			return false;
		if (partNumberRGC == null) {
			if (other.partNumberRGC != null)
				return false;
		} else if (!partNumberRGC.equals(other.partNumberRGC))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ItemBean [id=" + id + ", cliente=" + cliente
				+ ", departamento=" + departamento + ", ncmPreClassificado="
				+ ncmPreClassificado + ", fabricante=" + fabricante
				+ ", partNumber=" + partNumber + ", partNumberRGC="
				+ partNumberRGC + ", partNumberFabricante="
				+ partNumberFabricante + ", descricao=" + descricao
				+ ", dataRecebimento=" + dataRecebimento + ", dataCadastro="
				+ dataCadastro + ", status=" + status + ", classificacoes="
				+ classificacoes + ", catalogos=" + catalogos + "]";
	}
}