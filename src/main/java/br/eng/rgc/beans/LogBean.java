package br.eng.rgc.beans;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Bean com atributos dos Logs.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */
@Entity
@Table(name = "RGC_LOG")
public class LogBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_LOG",nullable=false)
	private Long id;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=UsuarioBean.class)
	@JoinColumn(name="ID_USUARIO",nullable=false)
	private UsuarioBean usuario;

	@Column(name="S_DESCRICAO",nullable=false)
	private String descricao;

	@Column(name="D_OPERACAO",nullable=false)
	private Date dataOperacao;

    /**
     *  Construtor vazio.
     */
	public LogBean(){
		this.id = new Long(0);
		this.usuario = new UsuarioBean();
		this.descricao = "";
		this.dataOperacao = new Date(0);
	}
    
	/**
	 *  Construtor com parâmetros.
	 *  
	 * @param id Id do Log.
	 * @param usuario Usuário.
	 * @param descricao Descrição da operação realizada.
	 * @param dataOperacao Data da operação realizada.
	 */
	public LogBean(Long id, UsuarioBean usuario, String descricao, Date dataOperacao) {
		this.id = id;
		this.usuario = usuario;
		this.descricao = descricao;
		this.dataOperacao = dataOperacao;
	}

	/**
	 * Recupera Id do Log.
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Define o Id do Log.
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Recupera Usuário.
	 * @return usuario
	 */
	public UsuarioBean getUsuario() {
		return usuario;
	}
	/**
	 * Define o Usuário.
	 * @param usuario
	 */
	public void setUsuario(UsuarioBean usuario) {
		this.usuario = usuario;
	}

	/**
	 * Recupera Descrição da operação realizada.	
	 * @return descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * Define a Descrição da operação realizada.	
     * @param descricao
     */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Recupera Data da operação realizada.	
	 * @return dataOperacao
	 */
	public Date getDataOperacao() {
		return dataOperacao;
	}
	/**
	 * Define a Data da operação realizada.	
     * @param dataOperacao
     */
	public void setDataOperacao(Date dataOperacao) {
		this.dataOperacao = dataOperacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dataOperacao == null) ? 0 : dataOperacao.hashCode());
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LogBean other = (LogBean) obj;
		if (dataOperacao == null) {
			if (other.dataOperacao != null)
				return false;
		} else if (!dataOperacao.equals(other.dataOperacao))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "LogBean [id=" + id + ", usuario=" + usuario + ", descricao="
				+ descricao + ", dataOperacao=" + dataOperacao + "]";
	}
}