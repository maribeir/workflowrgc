package br.eng.rgc.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Bean com atributos dos Clientes.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */
@Entity
@Table(name = "RGC_CLIENTE")
public class ClienteBean extends BaseBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_CLIENTE",nullable=false)
	private Long id;

	@Column(name="S_RAZAO_SOCIAL",nullable=false)
	private String razaoSocial;

	@Column(name="S_CNPJ",nullable=false)
	private String cnpj;

	@Column(name="S_WEBSITE",nullable=true)
	private String website;

    /**
     *  Construtor vazio.
     */
	public ClienteBean(){
		this.id = new Long(0);
		this.razaoSocial = "";
		this.cnpj = "";
		this.website = "";
	}
    
	/**
	 *  Construtor com parâmetros.
	 *  
	 * @param id Id do Cliente.
	 * @param razaoSocial Razão Social do Cliente.
	 * @param cnpj CNPJ do Cliente.
	 * @param website Website do Cliente.
	 */
	public ClienteBean(Long id, String razaoSocial, String cnpj, String website) {
		this.id = id;
		this.razaoSocial = razaoSocial;
		this.cnpj = cnpj;
		this.website = website;
	}

	/**
	 * Recupera Id do Cliente.
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Define o Id do Cliente.
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Recupera Razão Social do Cliente.	
	 * @return razaoSocial
	 */
	public String getRazaoSocial() {
		return razaoSocial;
	}
	/**
	 * Define a Razão Social do Cliente.	
     * @param razaoSocial
     */
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	/**
	 * Recupera CNPJ do Cliente.	
	 * @return cnpj
	 */
	public String getCNPJ() {
		return cnpj;
	}
	/**
	 * Define o CNPJ do Cliente.	
     * @param cnpj
     */
	public void setCNPJ(String cnpj) {
		this.cnpj = cnpj;
	}

	/**
	 * Recupera Website do Cliente.	
	 * @return website
	 */
	public String getWebsite() {
		return website;
	}
	/**
	 * Define o Website do Cliente.	
     * @param website
     */
	public void setWebsite(String website) {
		this.website = website;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cnpj == null) ? 0 : cnpj.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((razaoSocial == null) ? 0 : razaoSocial.hashCode());
		result = prime * result + ((website == null) ? 0 : website.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClienteBean other = (ClienteBean) obj;
		if (cnpj == null) {
			if (other.cnpj != null)
				return false;
		} else if (!cnpj.equals(other.cnpj))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (razaoSocial == null) {
			if (other.razaoSocial != null)
				return false;
		} else if (!razaoSocial.equals(other.razaoSocial))
			return false;
		if (website == null) {
			if (other.website != null)
				return false;
		} else if (!website.equals(other.website))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ClienteBean [id=" + id + ", razaoSocial=" + razaoSocial
				+ ", cnpj=" + cnpj + ", website=" + website + "]";
	}
}