package br.eng.rgc.beans;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.CascadeType;

/**
 * Bean com atributos dos NCMs.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */
@Entity
@Table(name = "RGC_NCM")
public class NCMBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_NCM",nullable=false)
	private Long id;

	@Column(name="S_DESCRICAO",nullable=false)
	private String descricao;

	@Column(name="S_NCM",nullable=false)
	private String ncm;

	@Column(name="N_ALIQUOTA",nullable=true)
	private Double aliquota;

	@Column(name="S_NOTA",nullable=true)
	private String nota;	

	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="ncm")
	private Collection<AtributoNVEBean> atributosNVE;

	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="ncm")
	private Collection<ClassificacaoBean> classificacoes;

	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="ncm")
	private Collection<DestaqueBean> destaques;

	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="ncm")
	private Collection<EXBean> exs;

	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="ncmPreClassificado")
	private Collection<ItemBean> itens;

    /**
     *  Construtor vazio.
     */
	public NCMBean(){
		this.id = new Long(0);
		this.descricao = "";
		this.ncm = "";
		this.aliquota = 0.0;
		this.nota = "";
		this.atributosNVE = new ArrayList<AtributoNVEBean>();
		this.classificacoes = new ArrayList<ClassificacaoBean>();
		this.destaques = new ArrayList<DestaqueBean>();
		this.exs = new ArrayList<EXBean>();
		this.itens = new ArrayList<ItemBean>();
	}
    
	/**
	 *  Construtor com parâmetros.
	 *  
	 * @param id Id do NCM.
	 * @param descricao Descrição do NCM.
	 * @param ncm Código do NCM.
	 * @param aliquota Alíquota do NCM.
	 * @param nota Nota do NCM (BK, BIT, #, §, **).
	 */
	public NCMBean(Long id, String descricao, String ncm, Double aliquota, String nota, Collection<AtributoNVEBean> atributosNVE,
			       Collection<ClassificacaoBean> classificacoes, Collection<DestaqueBean> destaques,
			       Collection<EXBean> exs, Collection<ItemBean> itens) {
		this.id = id;
		this.descricao = descricao;
		this.ncm = ncm;
		this.aliquota = aliquota;
		this.nota = nota;
		this.atributosNVE = atributosNVE;
		this.classificacoes = classificacoes;
		this.destaques = destaques;
		this.exs = exs;
		this.itens = itens;
	}

	/**
	 * Recupera Id do NCM.
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Define o Id do NCM.
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Recupera Descrição do NCM.	
	 * @return descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * Define a Descrição do NCM.	
     * @param descricao
     */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Recupera Código do NCM.	
	 * @return ncm
	 */
	public String getNCM() {
		return ncm;
	}
	/**
	 * Define o Código do NCM.	
     * @param ncm
     */
	public void setNCM(String ncm) {
		this.ncm = ncm;
	}

	/**
	 * Recupera Alíquota do NCM.	
	 * @return aliquota
	 */
	public Double getAliquota() {
		return aliquota;
	}
	/**
	 * Define a Alíquota do NCM.	
     * @param aliquota
     */
	public void setAliquota(Double aliquota) {
		this.aliquota = aliquota;
	}

	/**
	 * Recupera Nota do NCM (BK, BIT, #, §, **).	
	 * @return nota
	 */
	public String getNota() {
		return nota;
	}
	/**
	 * Define a Nota do NCM (BK, BIT, #, §, **).	
     * @param nota
     */
	public void setNota(String nota) {
		this.nota = nota;
	}

	/**
	 * Recupera Atributos NVE.	
	 * @return atributoNVE
	 */
	public Collection<AtributoNVEBean> getAtributosNVE() {
		return atributosNVE;
	}
	/**
	 * Define Atributos NVE.	
     * @param atributoNVE
     */
	public void setAtributosNVE(Collection<AtributoNVEBean> atributoNVE) {
		this.atributosNVE = atributoNVE;
	}

	/**
	 * Recupera Classificações.	
	 * @return classificacoes
	 */
	public Collection<ClassificacaoBean> getClassificacoes() {
		return classificacoes;
	}
	/**
	 * Define Classificações.	
     * @param classificacoes
     */
	public void setClassificacoes(Collection<ClassificacaoBean> classificacoes) {
		this.classificacoes = classificacoes;
	}

	/**
	 * Recupera Destaques.	
	 * @return destaques
	 */
	public Collection<DestaqueBean> getDestaques() {
		return destaques;
	}
	/**
	 * Define Destaques.	
     * @param destaques
     */
	public void setDestaques(Collection<DestaqueBean> destaques) {
		this.destaques = destaques;
	}

	/**
	 * Recupera EXs.	
	 * @return exs
	 */
	public Collection<EXBean> getExs() {
		return exs;
	}
	/**
	 * Define EXs.	
     * @param exs
     */
	public void setExs(Collection<EXBean> exs) {
		this.exs = exs;
	}

	/**
	 * Recupera Itens.	
	 * @return itens
	 */
	public Collection<ItemBean> getItens() {
		return itens;
	}
	/**
	 * Define Itens.	
     * @param itens
     */
	public void setItens(Collection<ItemBean> itens) {
		this.itens = itens;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((aliquota == null) ? 0 : aliquota.hashCode());
		result = prime * result
				+ ((atributosNVE == null) ? 0 : atributosNVE.hashCode());
		result = prime * result
				+ ((classificacoes == null) ? 0 : classificacoes.hashCode());
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result
				+ ((destaques == null) ? 0 : destaques.hashCode());
		result = prime * result + ((exs == null) ? 0 : exs.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((itens == null) ? 0 : itens.hashCode());
		result = prime * result + ((ncm == null) ? 0 : ncm.hashCode());
		result = prime * result + ((nota == null) ? 0 : nota.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NCMBean other = (NCMBean) obj;
		if (aliquota == null) {
			if (other.aliquota != null)
				return false;
		} else if (!aliquota.equals(other.aliquota))
			return false;
		if (atributosNVE == null) {
			if (other.atributosNVE != null)
				return false;
		} else if (!atributosNVE.equals(other.atributosNVE))
			return false;
		if (classificacoes == null) {
			if (other.classificacoes != null)
				return false;
		} else if (!classificacoes.equals(other.classificacoes))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (destaques == null) {
			if (other.destaques != null)
				return false;
		} else if (!destaques.equals(other.destaques))
			return false;
		if (exs == null) {
			if (other.exs != null)
				return false;
		} else if (!exs.equals(other.exs))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (itens == null) {
			if (other.itens != null)
				return false;
		} else if (!itens.equals(other.itens))
			return false;
		if (ncm == null) {
			if (other.ncm != null)
				return false;
		} else if (!ncm.equals(other.ncm))
			return false;
		if (nota == null) {
			if (other.nota != null)
				return false;
		} else if (!nota.equals(other.nota))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NCMBean [id=" + id + ", descricao=" + descricao + ", ncm="
				+ ncm + ", aliquota=" + aliquota + ", nota=" + nota
				+ ", atributosNVE=" + atributosNVE + ", classificacoes="
				+ classificacoes + ", destaques=" + destaques + ", exs=" + exs
				+ ", itens=" + itens + "]";
	}
}