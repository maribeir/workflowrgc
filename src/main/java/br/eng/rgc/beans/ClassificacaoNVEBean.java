package br.eng.rgc.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Bean com atributos dos NVEs utilizados em Classificações.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */
@Entity
@Table(name = "RGC_CLASSIFICACAO_NVE")
public class ClassificacaoNVEBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_CLASSIFICACAO_NVE",nullable=false)
	private Long id;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=ClassificacaoBean.class)
	@JoinColumn(name="ID_CLASSIFICACAO",nullable=false)
	private ClassificacaoBean classificacao;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=ValorNVEBean.class)
	@JoinColumn(name="ID_VALOR_NVE",nullable=false)
	private ValorNVEBean valorNVE;

    /**
     *  Construtor vazio.
     */
	public ClassificacaoNVEBean(){
		this.id = new Long(0);
		this.classificacao = new ClassificacaoBean();
		this.valorNVE = new ValorNVEBean();
	}
    
	/**
	 *  Construtor com parâmetros.
	 *  
	 * @param id Id do vínculo entre Classificação e NVE utilizado.
	 * @param classificacao Classificação.
	 * @param valorNVE Valor do NVE.
	 */
	public ClassificacaoNVEBean(Long id, ClassificacaoBean classificacao, ValorNVEBean valorNVE) {
		this.id = id;
		this.classificacao = classificacao;
		this.valorNVE = valorNVE;
	}

	/**
	 * Recupera Id do vínculo entre Classificação e NVE utilizado.
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Define o Id do vínculo entre Classificação e NVE utilizado.
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Recupera Classificação.
	 * @return classificacao
	 */
	public ClassificacaoBean getClassificacao() {
		return classificacao;
	}
	/**
	 * Define a Classificação.
	 * @param classificacao
	 */
	public void setClassificacao(ClassificacaoBean classificacao) {
		this.classificacao = classificacao;
	}

	/**
	 * Recupera Valor do NVE.
	 * @return valorNVE
	 */
	public ValorNVEBean getValorNVE() {
		return valorNVE;
	}
	/**
	 * Define o Id do NVE.
	 * @param idClassificacaoNVE
	 */
	public void setValorNVE(ValorNVEBean valorNVE) {
		this.valorNVE = valorNVE;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((classificacao == null) ? 0 : classificacao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((valorNVE == null) ? 0 : valorNVE.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClassificacaoNVEBean other = (ClassificacaoNVEBean) obj;
		if (classificacao == null) {
			if (other.classificacao != null)
				return false;
		} else if (!classificacao.equals(other.classificacao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (valorNVE == null) {
			if (other.valorNVE != null)
				return false;
		} else if (!valorNVE.equals(other.valorNVE))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ClassificacaoNVEBean [id=" + id + ", classificacao="
				+ classificacao + ", valorNVE=" + valorNVE + "]";
	}
}