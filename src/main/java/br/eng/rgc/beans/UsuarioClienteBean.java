package br.eng.rgc.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Bean com atributos dos vínculos entre Usuário e Cliente.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */
@Entity
@Table(name = "RGC_USUARIO_CLIENTE")
public class UsuarioClienteBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_USUARIO_CLIENTE",nullable=false)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY, targetEntity=UsuarioBean.class)
    @JoinColumn(name = "ID_USUARIO")
	private UsuarioBean usuario;
	
	@ManyToOne(fetch = FetchType.LAZY, targetEntity=ClienteBean.class)
    @JoinColumn(name = "ID_CLIENTE")
	private ClienteBean cliente;

    /**
     *  Construtor vazio.
     */
	public UsuarioClienteBean(){
		this.id = new Long(0);
		this.usuario = new UsuarioBean();
		this.cliente = new ClienteBean();
	}
    
	/**
	 *  Construtor com parâmetros.
	 *  
	 * @param id Id do vínculo entre Usuário e Cliente.
	 * @param usuarioBean Id da Usuário.
	 * @param clienteBean Id do Cliente.
	 */
	public UsuarioClienteBean(Long id, UsuarioBean usuario, ClienteBean cliente) {
		this.id = id;
		this.usuario = usuario;
		this.cliente = cliente;
	}

	/**
	 * Recupera Id do vínculo entre Usuário e Cliente.
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Define o Id do vínculo entre Usuário e Cliente.
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Recupera Usuário.
	 * @return usuario
	 */
	public UsuarioBean getUsuario() {
		return usuario;
	}
	/**
	 * Define o Usuário.
	 * @param usuario
	 */
	public void setUsuario(UsuarioBean usuario) {
		this.usuario = usuario;
	}

	/**
	 * Recupera Cliente.
	 * @return cliente
	 */
	public ClienteBean getCliente() {
		return cliente;
	}
	/**
	 * Define o Cliente.
	 * @param cliente
	 */
	public void setCliente(ClienteBean cliente) {
		this.cliente = cliente;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cliente == null) ? 0 : cliente.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioClienteBean other = (UsuarioClienteBean) obj;
		if (cliente == null) {
			if (other.cliente != null)
				return false;
		} else if (!cliente.equals(other.cliente))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UsuarioClienteBean [id=" + id + ", usuario=" + usuario
				+ ", cliente=" + cliente + "]";
	}
}