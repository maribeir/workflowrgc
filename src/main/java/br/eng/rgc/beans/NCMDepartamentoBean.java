package br.eng.rgc.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Bean com atributos dos NCMs dos Departamentos.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */
@Entity
@Table(name = "RGC_NCM_DEPARTAMENTO")
public class NCMDepartamentoBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_NCM_DEPARTAMENTO",nullable=false)
	private Long id;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=DepartamentoBean.class)
	@JoinColumn(name="ID_DEPARTAMENTO",nullable=false)
	private DepartamentoBean departamento;

	@Column(name="S_RAIZ_NCM",nullable=false)
	private String raizNCM;

    /**
     *  Construtor vazio.
     */
	public NCMDepartamentoBean(){
		this.id = new Long(0);
		this.departamento = new DepartamentoBean();
		this.raizNCM = "";
	}
    
	/**
	 *  Construtor com parâmetros.
	 *  
	 * @param id Id do NCM do Departamento.
	 * @param departamento Departamento.
	 * @param raizNCM Raiz do NCM.
	 */
	public NCMDepartamentoBean(Long id, DepartamentoBean departamento, String raizNCM) {
		this.id = id;
		this.departamento = departamento;
		this.raizNCM = raizNCM;
	}

	/**
	 * Recupera Id do NCM do Departamento.
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Define o Id do NCM do Departamento.
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Recupera Departamento.
	 * @return departamento
	 */
	public DepartamentoBean getDepartamento() {
		return departamento;
	}
	/**
	 * Define o Departamento.
	 * @param departamento
	 */
	public void setDepartamento(DepartamentoBean departamento) {
		this.departamento = departamento;
	}

	/**
	 * Recupera Raiz do NCM.	
	 * @return raizNCM
	 */
	public String getRaizNCM() {
		return raizNCM;
	}
	/**
	 * Define a Raiz do NCM.	
     * @param raizNCM
     */
	public void setRaizNCM(String raizNCM) {
		this.raizNCM = raizNCM;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((departamento == null) ? 0 : departamento.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((raizNCM == null) ? 0 : raizNCM.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NCMDepartamentoBean other = (NCMDepartamentoBean) obj;
		if (departamento == null) {
			if (other.departamento != null)
				return false;
		} else if (!departamento.equals(other.departamento))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (raizNCM == null) {
			if (other.raizNCM != null)
				return false;
		} else if (!raizNCM.equals(other.raizNCM))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NCMDepartamentoBean [id=" + id + ", departamento="
				+ departamento + ", raizNCM=" + raizNCM + "]";
	}
}