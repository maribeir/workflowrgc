package br.eng.rgc.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Bean com atributos dos Catálogos dos Itens.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */
@Entity
@Table(name = "RGC_ITEM_CATALOGO")
public class ItemCatalogoBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_ITEM_CATALOGO",nullable=false)
	private Long id;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=ItemBean.class)
	@JoinColumn(name="ID_ITEM",nullable=false)
	private ItemBean item;

	@Column(name="S_ARQUIVO",nullable=true)
	private String arquivo;

	@Column(name="S_LINK",nullable=true)
	private String link;

    /**
     *  Construtor vazio.
     */
	public ItemCatalogoBean(){
		this.id = new Long(0);
		this.item = new ItemBean();
		this.arquivo = "";
		this.link = "";
	}
    
	/**
	 *  Construtor com parâmetros.
	 *  
	 * @param id Id do Catálogo do Item.
	 * @param item Item.
	 * @param arquivo Arquivo.
	 * @param link Link.
	 */
	public ItemCatalogoBean(Long id, ItemBean item, String arquivo, String link) {
		this.id = id;
		this.item = item;
		this.arquivo = arquivo;
		this.link = link;
	}

	/**
	 * Recupera Id do Catálogo do Item.
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Define o Id do Catálogo do Item.
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Recupera Item.
	 * @return item
	 */
	public ItemBean getItem() {
		return item;
	}
	/**
	 * Define o Item.
	 * @param item
	 */
	public void setItem(ItemBean item) {
		this.item = item;
	}

	/**
	 * Recupera Arquivo.	
	 * @return arquivo
	 */
	public String getArquivo() {
		return arquivo;
	}
	/**
	 * Define o Arquivo.	
     * @param arquivo
     */
	public void setArquivo(String arquivo) {
		this.arquivo = arquivo;
	}

	/**
	 * Recupera Link.	
	 * @return link
	 */
	public String getLink() {
		return link;
	}
	/**
	 * Define o Link.	
     * @param link
     */
	public void setLink(String link) {
		this.link = link;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((arquivo == null) ? 0 : arquivo.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((item == null) ? 0 : item.hashCode());
		result = prime * result + ((link == null) ? 0 : link.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemCatalogoBean other = (ItemCatalogoBean) obj;
		if (arquivo == null) {
			if (other.arquivo != null)
				return false;
		} else if (!arquivo.equals(other.arquivo))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (item == null) {
			if (other.item != null)
				return false;
		} else if (!item.equals(other.item))
			return false;
		if (link == null) {
			if (other.link != null)
				return false;
		} else if (!link.equals(other.link))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ItemCatalogoBean [id=" + id + ", item=" + item + ", arquivo="
				+ arquivo + ", link=" + link + "]";
	}
}