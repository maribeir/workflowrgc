package br.eng.rgc.beans;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Bean com atributos dos EX tarifários.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */
@Entity
@Table(name = "RGC_EX")
public class EXBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_EX",nullable=false)
	private Long id;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=NCMBean.class)
	@JoinColumn(name="ID_NCM",nullable=false)
	private NCMBean ncm;

	@Column(name="S_DESCRICAO",nullable=false)
	private String descricao;

	@Column(name="S_EX",nullable=false)
	private String ex;

	@Column(name="N_ALIQUOTA",nullable=true)
	private Double aliquota;

	@Column(name="S_TIPO",nullable=false)
	private String tipo;
	
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="exii")
	private Collection<ClassificacaoBean> classificacoesii;
	
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="exipi")
	private Collection<ClassificacaoBean> classificacoesipi;

    /**
     *  Construtor vazio.
     */
	public EXBean(){
		this.id = new Long(0);
		this.ncm = new NCMBean();
		this.descricao = "";
		this.ex = "";
		this.aliquota = 0.0;
		this.classificacoesii = new ArrayList<ClassificacaoBean>();
		this.classificacoesipi = new ArrayList<ClassificacaoBean>();
	}
    
	/**
	 *  Construtor com parâmetros.
	 *  
	 * @param id Id do EX.
	 * @param ncm NCM.
	 * @param descricao Descrição do EX.
	 * @param ex Código do EX.
	 * @param aliquota Alíquota do EX.
	 * @param tipo Tipo do EX.
	 */
	public EXBean(Long id, NCMBean ncm, String descricao, String ex, Double aliquota, String tipo,
			      Collection<ClassificacaoBean> classificacoesii, Collection<ClassificacaoBean> classificacoesipi) {
		this.id = id;
		this.ncm = ncm;
		this.descricao = descricao;
		this.ex = ex;
		this.aliquota = aliquota;
		this.tipo = tipo;
		this.classificacoesii = classificacoesii;
		this.classificacoesipi = classificacoesipi;
	}

	/**
	 * Recupera Id do EX.
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Define o Id do EX.
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Recupera NCM.
	 * @return ncm
	 */
	public NCMBean getNcm() {
		return ncm;
	}
	/**
	 * Define o NCM.
	 * @param ncm
	 */
	public void setNcm(NCMBean ncm) {
		this.ncm = ncm;
	}

	/**
	 * Recupera Descrição do EX.	
	 * @return descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * Define a Descrição do EX.	
     * @param descricao
     */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Recupera Código do EX.	
	 * @return destaque
	 */
	public String getEX() {
		return ex;
	}
	/**
	 * Define o Código do EX.	
     * @param destaque
     */
	public void setEX(String ex) {
		this.ex = ex;
	}

	/**
	 * Recupera Alíquota do EX.	
	 * @return aliquota
	 */
	public Double getAliquota() {
		return aliquota;
	}
	/**
	 * Define a Alíquota do EX.	
     * @param aliquota
     */
	public void setAliquota(Double aliquota) {
		this.aliquota = aliquota;
	}

	/**
	 * Recupera Tipo do EX.	
	 * @return tipo
	 */
	public String getTipo() {
		return tipo;
	}
	/**
	 * Define o Tipo do EX.	
     * @param tipo
     */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Recupera Classificações.	
	 * @return classificacoesii
	 */
	public Collection<ClassificacaoBean> getClassificacoesii() {
		return classificacoesii;
	}
	/**
	 * Define Classificações.	
     * @param classificacoesii
     */
	public void setClassificacoesii(Collection<ClassificacaoBean> classificacoesii) {
		this.classificacoesii = classificacoesii;
	}

	/**
	 * Recupera Classificações.	
	 * @return classificacoesipi
	 */
	public Collection<ClassificacaoBean> getClassificacoesipi() {
		return classificacoesipi;
	}
	/**
	 * Define Classificações.	
     * @param classificacoesipi
     */
	public void setClassificacoesipi(Collection<ClassificacaoBean> classificacoesipi) {
		this.classificacoesipi = classificacoesipi;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((aliquota == null) ? 0 : aliquota.hashCode());
		result = prime
				* result
				+ ((classificacoesii == null) ? 0 : classificacoesii.hashCode());
		result = prime
				* result
				+ ((classificacoesipi == null) ? 0 : classificacoesipi
						.hashCode());
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((ex == null) ? 0 : ex.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ncm == null) ? 0 : ncm.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EXBean other = (EXBean) obj;
		if (aliquota == null) {
			if (other.aliquota != null)
				return false;
		} else if (!aliquota.equals(other.aliquota))
			return false;
		if (classificacoesii == null) {
			if (other.classificacoesii != null)
				return false;
		} else if (!classificacoesii.equals(other.classificacoesii))
			return false;
		if (classificacoesipi == null) {
			if (other.classificacoesipi != null)
				return false;
		} else if (!classificacoesipi.equals(other.classificacoesipi))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (ex == null) {
			if (other.ex != null)
				return false;
		} else if (!ex.equals(other.ex))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ncm == null) {
			if (other.ncm != null)
				return false;
		} else if (!ncm.equals(other.ncm))
			return false;
		if (tipo == null) {
			if (other.tipo != null)
				return false;
		} else if (!tipo.equals(other.tipo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "EXBean [id=" + id + ", ncm=" + ncm + ", descricao=" + descricao
				+ ", ex=" + ex + ", aliquota=" + aliquota + ", tipo=" + tipo
				+ ", classificacoesii=" + classificacoesii
				+ ", classificacoesipi=" + classificacoesipi + "]";
	}
}