package br.eng.rgc.beans;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Bean com atributos dos Departamentos.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 07/2017
 * 
 */
@Entity
@Table(name = "RGC_USUARIO")
public class UsuarioBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_USUARIO",nullable=false)
	private Long id;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=CargoBean.class)
	@JoinColumn(name="ID_CARGO",nullable=false)
	private CargoBean cargo;

	@Column(name="S_NOME",nullable=false)
	private String nome;

	@Column(name="S_CPF",nullable=false)
	private String cpf;

	@Column(name="S_EMAIL",nullable=false)
	private String email;

	@Column(name="S_TELEFONE",nullable=true)
	private String telefone;

	@Column(name="S_USERNAME",nullable=false)
	private String username;

	@Column(name="S_SENHA",nullable=false)
	private String senha;

	@Column(name="S_ATIVO",nullable=false)
	private String ativo;

	@Column(name="S_DISPONIVEL",nullable=true)
	private String disponivel;

	@Column(name="D_CADASTRO",nullable=false)
	private Date dataCadastro;
	
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="usuario")
	private Collection<ClassificacaoBean> classificacoes;
	
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="usuario")
	private Collection<IndispUsuarioBean> usuariosIndisponiveis;
	
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="usuarioSubstituto")
	private Collection<IndispUsuarioBean> usuariosSubstitutos;
	
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="usuarioAprovador")
	private Collection<IndispUsuarioBean> usuariosAprovadores;
	
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="usuario")
	private Collection<LogBean> logs;
	
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="usuario")
	private Collection<UsuarioClienteBean> usuariosClientes;
	
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="usuario")
	private Collection<UsuarioDepartamentoBean> usuariosDepartamentos;

    /**
     *  Construtor vazio.
     */
	public UsuarioBean(){
		this.id = new Long(0);
		this.cargo = new CargoBean();
		this.nome = "";
		this.cpf = "";
		this.email = "";
		this.telefone = "";
		this.username = "";
		this.senha = "";
		this.ativo = "";
		this.disponivel = "";
		this.dataCadastro = new Date(0);
		this.classificacoes = new ArrayList<ClassificacaoBean>();
		this.usuariosIndisponiveis = new ArrayList<IndispUsuarioBean>();
		this.usuariosSubstitutos = new ArrayList<IndispUsuarioBean>();
		this.usuariosAprovadores = new ArrayList<IndispUsuarioBean>();
		this.logs = new ArrayList<LogBean>();
		this.usuariosClientes = new ArrayList<UsuarioClienteBean>();
		this.usuariosDepartamentos = new ArrayList<UsuarioDepartamentoBean>();
	}
    
	/**
	 *  Construtor com parâmetros.
	 *  
	 * @param id Id do Usuário.
	 * @param cargo Cargo.
	 * @param nome Nome do Usuário.
	 * @param cpf CPF do Usuário.
	 * @param email E-mail do Usuário.
	 * @param username Username do Usuário.
	 * @param telefone Telefone do Usuário.
	 * @param senha Senha do Usuário.
	 * @param ativo Flag Ativo (S ou N).
	 * @param disponivel Flag Disponível (S ou N).
	 * @param dataCadstro Data de Cadastro do Usuário.
	 */
	public UsuarioBean(Long id, CargoBean cargo, String nome, String cpf, String email, String username, String telefone,
			           String senha, String ativo, String disponivel, Date dataCadastro, Collection<ClassificacaoBean> classificacoes,
			           Collection<IndispUsuarioBean> usuariosIndisponiveis, Collection<IndispUsuarioBean> usuariosSubstitutos,
			           Collection<IndispUsuarioBean> usuariosAprovadores, Collection<LogBean> logs,
			           Collection<UsuarioClienteBean> usuariosClientes, Collection<UsuarioDepartamentoBean> usuariosDepartamentos) {
		this.id = id;
		this.cargo = cargo;
		this.nome = nome;
		this.cpf = cpf;
		this.email = email;
		this.username = username;
		this.telefone = telefone;
		this.senha = senha;
		this.ativo = ativo;
		this.disponivel = disponivel;
		this.dataCadastro = dataCadastro;
		this.classificacoes = classificacoes;
		this.usuariosIndisponiveis = usuariosIndisponiveis;
		this.usuariosSubstitutos = usuariosSubstitutos;
		this.usuariosAprovadores = usuariosAprovadores;
		this.logs = logs;
		this.usuariosClientes = usuariosClientes;
		this.usuariosDepartamentos = usuariosDepartamentos;
	}

	/**
	 * Recupera Id do Usuário.
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Define o Id do Usuário.
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Recupera Cargo.
	 * @return cargo
	 */
	public CargoBean getCargo() {
		return cargo;
	}
	/**
	 * Define o Cargo.
	 * @param cargo
	 */
	public void setCargo(CargoBean cargo) {
		this.cargo = cargo;
	}

	/**
	 * Recupera Nome do Usuário.	
	 * @return nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * Define o Nome do Usuário.	
     * @param nome
     */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Recupera CPF do Usuário.	
	 * @return cpf
	 */
	public String getCpf() {
		return cpf;
	}
	/**
	 * Define o CPF do Usuário.	
     * @param cpf
     */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	/**
	 * Recupera E-mail do Usuário.	
	 * @return email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * Define o E-mail do Usuário.	
     * @param cpf
     */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Recupera Telefone do Usuário.	
	 * @return telefone
	 */
	public String getTelefone() {
		return telefone;
	}
	/**
	 * Define o Telefone do Usuário.	
     * @param telefone
     */
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	/**
	 * Recupera Username do Usuário.	
	 * @return username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * Define o Username do Usuário.	
     * @param username
     */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Recupera Senha do Usuário.	
	 * @return senha
	 */
	public String getSenha() {
		return senha;
	}
	/**
	 * Define a Senha do Usuário.	
     * @param senha
     */
	public void setSenha(String senha) {
		this.senha = senha;
	}

	/**
	 * Recupera flag Ativo (S ou N).	
	 * @return ativo
	 */
	public String getAtivo() {
		return ativo;
	}
	/**
	 * Define a flag Ativo (S ou N).	
     * @param ativo
     */
	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}

	/**
	 * Recupera flag Disponível (S ou N).	
	 * @return disponivel
	 */
	public String getDisponivel() {
		return disponivel;
	}
	/**
	 * Define a flag Disponível (S ou N).	
     * @param disponivel
     */
	public void setDisponivel(String disponivel) {
		this.disponivel = disponivel;
	}

	/**
	 * Recupera Data de Cadastro do Usuário.	
	 * @return dataCadastro
	 */
	public Date getDataCadastro() {
		return dataCadastro;
	}
	/**
	 * Define a Data de Cadastro do Usuário.	
     * @param dataCadastro
     */
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	/**
	 * Recupera Classificações.	
	 * @return classificacoes
	 */
	public Collection<ClassificacaoBean> getClassificacoes() {
		return classificacoes;
	}
	/**
	 * Define Classificações.	
     * @param classificacoes
     */
	public void setClassificacoes(Collection<ClassificacaoBean> classificacoes) {
		this.classificacoes = classificacoes;
	}

	/**
	 * Recupera Usuários Indisponíveis.	
	 * @return usuariosIndisponiveis
	 */
	public Collection<IndispUsuarioBean> getUsuariosIndisponiveis() {
		return usuariosIndisponiveis;
	}
	/**
	 * Define Usuários Indisponíveis.	
     * @param usuariosIndisponiveis
     */
	public void setUsuariosIndisponiveis(Collection<IndispUsuarioBean> usuariosIndisponiveis) {
		this.usuariosIndisponiveis = usuariosIndisponiveis;
	}

	/**
	 * Recupera Usuários Substitutos.	
	 * @return usuariosSubstitutos
	 */
	public Collection<IndispUsuarioBean> getUsuariosSubstitutos() {
		return usuariosSubstitutos;
	}
	/**
	 * Define Usuários Substitutos.	
     * @param usuariosSubstitutos
     */
	public void setUsuariosSubstitutos(Collection<IndispUsuarioBean> usuariosSubstitutos) {
		this.usuariosSubstitutos = usuariosSubstitutos;
	}

	/**
	 * Recupera Usuários Aprovadores.	
	 * @return usuariosAprovadores
	 */
	public Collection<IndispUsuarioBean> getUsuariosAprovadores() {
		return usuariosAprovadores;
	}
	/**
	 * Define Usuários Aprovadores.	
     * @param usuariosAprovadores
     */
	public void setUsuariosAprovadores(Collection<IndispUsuarioBean> usuariosAprovadores) {
		this.usuariosAprovadores = usuariosAprovadores;
	}

	/**
	 * Recupera Logs.	
	 * @return logs
	 */
	public Collection<LogBean> getLogs() {
		return logs;
	}
	/**
	 * Define Logs.	
     * @param logs
     */
	public void setLogs(Collection<LogBean> logs) {
		this.logs = logs;
	}

	/**
	 * Recupera Usuários Clientes.	
	 * @return usuariosClientes
	 */
	public Collection<UsuarioClienteBean> getUsuariosClientes() {
		return usuariosClientes;
	}
	/**
	 * Define Usuários Clientes.	
     * @param usuariosClientes
     */
	public void setUsuariosClientes(Collection<UsuarioClienteBean> usuariosClientes) {
		this.usuariosClientes = usuariosClientes;
	}

	/**
	 * Recupera Usuários Departamentos.	
	 * @return usuariosDepartamentos
	 */
	public Collection<UsuarioDepartamentoBean> getUsuariosDepartamentos() {
		return usuariosDepartamentos;
	}
	/**
	 * Define Usuários Departamentos.	
     * @param usuariosDepartamentos
     */
	public void setUsuariosDepartamentos(Collection<UsuarioDepartamentoBean> usuariosDepartamentos) {
		this.usuariosDepartamentos = usuariosDepartamentos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ativo == null) ? 0 : ativo.hashCode());
		result = prime * result + ((cargo == null) ? 0 : cargo.hashCode());
		result = prime * result
				+ ((classificacoes == null) ? 0 : classificacoes.hashCode());
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		result = prime * result
				+ ((dataCadastro == null) ? 0 : dataCadastro.hashCode());
		result = prime * result
				+ ((disponivel == null) ? 0 : disponivel.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((logs == null) ? 0 : logs.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((senha == null) ? 0 : senha.hashCode());
		result = prime * result
				+ ((telefone == null) ? 0 : telefone.hashCode());
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		result = prime
				* result
				+ ((usuariosAprovadores == null) ? 0 : usuariosAprovadores
						.hashCode());
		result = prime
				* result
				+ ((usuariosClientes == null) ? 0 : usuariosClientes.hashCode());
		result = prime
				* result
				+ ((usuariosDepartamentos == null) ? 0 : usuariosDepartamentos
						.hashCode());
		result = prime
				* result
				+ ((usuariosIndisponiveis == null) ? 0 : usuariosIndisponiveis
						.hashCode());
		result = prime
				* result
				+ ((usuariosSubstitutos == null) ? 0 : usuariosSubstitutos
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioBean other = (UsuarioBean) obj;
		if (ativo == null) {
			if (other.ativo != null)
				return false;
		} else if (!ativo.equals(other.ativo))
			return false;
		if (cargo == null) {
			if (other.cargo != null)
				return false;
		} else if (!cargo.equals(other.cargo))
			return false;
		if (classificacoes == null) {
			if (other.classificacoes != null)
				return false;
		} else if (!classificacoes.equals(other.classificacoes))
			return false;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		if (dataCadastro == null) {
			if (other.dataCadastro != null)
				return false;
		} else if (!dataCadastro.equals(other.dataCadastro))
			return false;
		if (disponivel == null) {
			if (other.disponivel != null)
				return false;
		} else if (!disponivel.equals(other.disponivel))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (logs == null) {
			if (other.logs != null)
				return false;
		} else if (!logs.equals(other.logs))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (senha == null) {
			if (other.senha != null)
				return false;
		} else if (!senha.equals(other.senha))
			return false;
		if (telefone == null) {
			if (other.telefone != null)
				return false;
		} else if (!telefone.equals(other.telefone))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		if (usuariosAprovadores == null) {
			if (other.usuariosAprovadores != null)
				return false;
		} else if (!usuariosAprovadores.equals(other.usuariosAprovadores))
			return false;
		if (usuariosClientes == null) {
			if (other.usuariosClientes != null)
				return false;
		} else if (!usuariosClientes.equals(other.usuariosClientes))
			return false;
		if (usuariosDepartamentos == null) {
			if (other.usuariosDepartamentos != null)
				return false;
		} else if (!usuariosDepartamentos.equals(other.usuariosDepartamentos))
			return false;
		if (usuariosIndisponiveis == null) {
			if (other.usuariosIndisponiveis != null)
				return false;
		} else if (!usuariosIndisponiveis.equals(other.usuariosIndisponiveis))
			return false;
		if (usuariosSubstitutos == null) {
			if (other.usuariosSubstitutos != null)
				return false;
		} else if (!usuariosSubstitutos.equals(other.usuariosSubstitutos))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UsuarioBean [id=" + id + ", cargo=" + cargo + ", nome=" + nome
				+ ", cpf=" + cpf + ", email=" + email + ", telefone="
				+ telefone + ", username=" + username + ", senha=" + senha
				+ ", ativo=" + ativo + ", disponivel=" + disponivel
				+ ", dataCadastro=" + dataCadastro + ", classificacoes="
				+ classificacoes + ", usuariosIndisponiveis="
				+ usuariosIndisponiveis + ", usuariosSubstitutos="
				+ usuariosSubstitutos + ", usuariosAprovadores="
				+ usuariosAprovadores + ", logs=" + logs
				+ ", usuariosClientes=" + usuariosClientes
				+ ", usuariosDepartamentos=" + usuariosDepartamentos + "]";
	}
}