package br.eng.rgc.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Bean com atributos dos Contatos dos Clientes.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */
@Entity
@Table(name = "RGC_CLIENTE_CONTATO")
public class ClienteContatoBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_CLIENTE_CONTATO",nullable=false)
	private Long id;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=ClienteBean.class)
	@JoinColumn(name="ID_CLIENTE",nullable=false)
	private ClienteBean cliente;

	@Column(name="S_NOME",nullable=false)
	private String nome;

	@Column(name="S_EMAIL",nullable=true)
	private String email;

	@Column(name="S_TELEFONE",nullable=true)
	private String telefone;

	@Column(name="S_TELEFONE2",nullable=true)
	private String telefone2;

    /**
     *  Construtor vazio.
     */
	public ClienteContatoBean(){
		this.id = new Long(0);
		this.cliente = new ClienteBean();
		this.nome = "";
		this.email = "";
		this.telefone = "";
		this.telefone2 = "";
	}
    
	/**
	 *  Construtor com parâmetros.
	 *  
	 * @param id Id do Contato do Cliente.
	 * @param cliente Cliente.
	 * @param nome Nome do Contato do Cliente.
	 * @param email E-mail do Contato do Cliente.
	 * @param telefone Telefone do Contato do Cliente.
	 * @param telefone2 Telefone alternativo do Contato do Cliente.
	 */
	public ClienteContatoBean(Long id, ClienteBean cliente, String nome, String email, String telefone, String telefone2) {
		this.id = id;
		this.cliente = cliente;
		this.nome = nome;
		this.email = email;
		this.telefone = telefone;
		this.telefone2 = telefone2;
	}

	/**
	 * Recupera Id do Contato do Cliente.
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Define o Id do Contato do Cliente.
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Recupera Cliente.
	 * @return cliente
	 */
	public ClienteBean getCliente() {
		return cliente;
	}
	/**
	 * Define o Cliente.
	 * @param cliente
	 */
	public void setCliente(ClienteBean cliente) {
		this.cliente = cliente;
	}

	/**
	 * Recupera Nome do Contato do Cliente.	
	 * @return nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * Define o Nome do Contato do Cliente.	
     * @param nome
     */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Recupera E-mail do Contato do Cliente.	
	 * @return email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * Define o E-mail do Contato do Cliente.	
     * @param email
     */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Recupera Telefone do Contato do Cliente.	
	 * @return telefone
	 */
	public String getTelefone() {
		return telefone;
	}
	/**
	 * Define o Telefone do Contato do Cliente.	
     * @param telefone
     */
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	/**
	 * Recupera Telefone alternativo do Contato do Cliente.	
	 * @return telefone2
	 */
	public String getTelefone2() {
		return telefone2;
	}
	/**
	 * Define o Telefone alternativo do Contato do Cliente.	
     * @param telefone2
     */
	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cliente == null) ? 0 : cliente.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result
				+ ((telefone == null) ? 0 : telefone.hashCode());
		result = prime * result
				+ ((telefone2 == null) ? 0 : telefone2.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClienteContatoBean other = (ClienteContatoBean) obj;
		if (cliente == null) {
			if (other.cliente != null)
				return false;
		} else if (!cliente.equals(other.cliente))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (telefone == null) {
			if (other.telefone != null)
				return false;
		} else if (!telefone.equals(other.telefone))
			return false;
		if (telefone2 == null) {
			if (other.telefone2 != null)
				return false;
		} else if (!telefone2.equals(other.telefone2))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ClienteContatoBean [id=" + id + ", cliente=" + cliente
				+ ", nome=" + nome + ", email=" + email + ", telefone="
				+ telefone + ", telefone2=" + telefone2 + "]";
	}
}