package br.eng.rgc.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Bean com atributos dos vínculos entre Usuários e Departamentos.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */
@Entity
@Table(name = "RGC_USUARIO_DEPARTAMENTO")
public class UsuarioDepartamentoBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_USUARIO_DEPARTAMENTO",nullable=false)
	private Long id;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=UsuarioBean.class)
	@JoinColumn(name="ID_USUARIO",nullable=false)
	private UsuarioBean usuario;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=DepartamentoBean.class)
	@JoinColumn(name="ID_DEPARTAMENTO",nullable=false)
	private DepartamentoBean departamento;

    /**
     *  Construtor vazio.
     */
	public UsuarioDepartamentoBean(){
		this.id = new Long(0);
		this.usuario = new UsuarioBean();
		this.departamento = new DepartamentoBean();
	}
    
	/**
	 *  Construtor com parâmetros.
	 *  
	 * @param id Id do vínculo entre Classificação e NVE utilizado.
	 * @param usuario Usuário.
	 * @param departamento Departamento.
	 */
	public UsuarioDepartamentoBean(Long id, UsuarioBean usuario, DepartamentoBean departamento) {
		this.id = id;
		this.usuario = usuario;
		this.departamento = departamento;
	}

	/**
	 * Recupera Id do vínculo entre Classificação e NVE utilizado.
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Define o Id do vínculo entre Classificação e NVE utilizado.
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Recupera Usuário.
	 * @return usuario
	 */
	public UsuarioBean getUsuario() {
		return usuario;
	}
	/**
	 * Define o Usuário.
	 * @param usuario
	 */
	public void setUsuario(UsuarioBean usuario) {
		this.usuario = usuario;
	}

	/**
	 * Recupera Departamento.
	 * @return departamento
	 */
	public DepartamentoBean getDepartamento() {
		return departamento;
	}
	/**
	 * Define o Departamento.
	 * @param departamento
	 */
	public void setDepartamento(DepartamentoBean departamento) {
		this.departamento = departamento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((departamento == null) ? 0 : departamento.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioDepartamentoBean other = (UsuarioDepartamentoBean) obj;
		if (departamento == null) {
			if (other.departamento != null)
				return false;
		} else if (!departamento.equals(other.departamento))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UsuarioDepartamentoBean [id=" + id + ", usuario=" + usuario
				+ ", departamento=" + departamento + "]";
	}
}