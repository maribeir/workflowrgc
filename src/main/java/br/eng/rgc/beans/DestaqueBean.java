package br.eng.rgc.beans;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Bean com atributos dos Destaques.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 07/2017
 * 
 */
@Entity
@Table(name = "RGC_DESTAQUE")
public class DestaqueBean extends BaseBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_DESTAQUE",nullable=false)
	private Long id;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=NCMBean.class)
	@JoinColumn(name="ID_NCM",nullable=false)
	private NCMBean ncm;

	@Column(name="S_DESCRICAO",nullable=false)
	private String descricao;

	@Column(name="S_DESTAQUE",nullable=false)
	private String destaque;

	@Column(name="N_ALIQUOTA",nullable=true)
	private Double aliquota;
	
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="destaque")
	private Collection<ClassificacaoBean> classificacoes;

    /**
     *  Construtor vazio.
     */
	public DestaqueBean(){
		this.id = new Long(0);
		this.ncm = new NCMBean();
		this.descricao = "";
		this.destaque = "";
		this.aliquota = 0.0;
		this.classificacoes = new ArrayList<ClassificacaoBean>();
	}
    
	/**
	 *  Construtor com parâmetros.
	 *  
	 * @param id Id do Destaque.
	 * @param ncm NCM.
	 * @param descricao Descrição do Destaque.
	 * @param destaque Código do Destaque.
	 * @param aliquota Alíquota do Destaque.
	 */
	public DestaqueBean(Long id, NCMBean ncm, String descricao, String destaque, Double aliquota, Collection<ClassificacaoBean> classificacoes) {
		this.id = id;
		this.ncm = ncm;
		this.descricao = descricao;
		this.destaque = destaque;
		this.aliquota = aliquota;
		this.classificacoes = classificacoes;
	}

	/**
	 * Recupera Id do Destaque.
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Define o Id do Destaque.
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Recupera NCM.
	 * @return ncm
	 */
	public NCMBean getNcm() {
		return ncm;
	}
	/**
	 * Define o NCM.
	 * @param ncm
	 */
	public void setNcm(NCMBean ncm) {
		this.ncm = ncm;
	}

	/**
	 * Recupera Descrição do Destaque.	
	 * @return descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * Define a Descrição do Destaque.	
     * @param descricao
     */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Recupera Código do Destaque.	
	 * @return destaque
	 */
	public String getDestaque() {
		return destaque;
	}
	/**
	 * Define o Código do Destaque.	
     * @param destaque
     */
	public void setDestaque(String destaque) {
		this.destaque = destaque;
	}

	/**
	 * Recupera Alíquota do Destaque.	
	 * @return aliquota
	 */
	public Double getAliquota() {
		return aliquota;
	}
	/**
	 * Define a Alíquota do Destaque.	
     * @param aliquota
     */
	public void setAliquota(Double aliquota) {
		this.aliquota = aliquota;
	}

	/**
	 * Recupera Classificações.	
	 * @return classificacoes
	 */
	public Collection<ClassificacaoBean> getClassificacoes() {
		return classificacoes;
	}
	/**
	 * Define Classificações.	
     * @param classificacoes
     */
	public void setClassificacoes(Collection<ClassificacaoBean> classificacoes) {
		this.classificacoes = classificacoes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((aliquota == null) ? 0 : aliquota.hashCode());
		result = prime * result
				+ ((classificacoes == null) ? 0 : classificacoes.hashCode());
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result
				+ ((destaque == null) ? 0 : destaque.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ncm == null) ? 0 : ncm.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DestaqueBean other = (DestaqueBean) obj;
		if (aliquota == null) {
			if (other.aliquota != null)
				return false;
		} else if (!aliquota.equals(other.aliquota))
			return false;
		if (classificacoes == null) {
			if (other.classificacoes != null)
				return false;
		} else if (!classificacoes.equals(other.classificacoes))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (destaque == null) {
			if (other.destaque != null)
				return false;
		} else if (!destaque.equals(other.destaque))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ncm == null) {
			if (other.ncm != null)
				return false;
		} else if (!ncm.equals(other.ncm))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DestaqueBean [id=" + id + ", ncm=" + ncm + ", descricao="
				+ descricao + ", destaque=" + destaque + ", aliquota="
				+ aliquota + ", classificacoes=" + classificacoes + "]";
	}
}