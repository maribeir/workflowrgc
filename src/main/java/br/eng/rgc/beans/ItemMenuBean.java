package br.eng.rgc.beans;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Bean com atributos dos Itens de Menu.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */
@Entity
@Table(name = "SEC_ITEM_MENU")
public class ItemMenuBean extends BaseBean {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ID_ITEM_MENU",nullable=false)
	private Long id;	

	@OneToMany(mappedBy="itemMenuSECPai",targetEntity=ItemMenuBean.class, fetch=FetchType.LAZY,cascade = CascadeType.ALL)
	private List<ItemMenuBean> subMenus;
	
	@JoinColumn(name="ID_ITEM_MENU_PAI",nullable=true)
	@ManyToOne(fetch = FetchType.LAZY, targetEntity=ItemMenuBean.class)	
	private ItemMenuBean itemMenuSECPai;	 

	@Column(name="N_NIVEL")
	private int nivel;
	
	@Column(name="N_ORDEM")
	private int ordem;
	
	@Column(name="S_PAGINA")
	private String pagina;
	
	@Column(name="S_NOME_BASE")
	private String nomeBase;
	
	@Column(name="S_ATIVO")
	private String ativo;
	
	public ItemMenuBean()
	{
		this.id = new Long(0);
		this.subMenus = new ArrayList<ItemMenuBean>();
		this.itemMenuSECPai = null;
		this.nivel = 0;
		this.ordem = 0;
		this.pagina = "";
		this.nomeBase = "";
		this.ativo = "";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<ItemMenuBean> getSubMenus() {
		return subMenus;
	}

	public void setSubMenus(List<ItemMenuBean> subMenus) {
		this.subMenus = subMenus;
	}

	public ItemMenuBean getItemMenuSECPai() {
		return itemMenuSECPai;
	}

	public void setItemMenuSECPai(ItemMenuBean itemMenuSECPai) {
		this.itemMenuSECPai = itemMenuSECPai;
	}

	public int getNivel() {
		return nivel;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
	}

	public int getOrdem() {
		return ordem;
	}

	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}

	public String getPagina() {
		return pagina;
	}

	public void setPagina(String pagina) {
		this.pagina = pagina;
	}

	public String getNomeBase() {
		return nomeBase;
	}

	public void setNomeBase(String nomeBase) {
		this.nomeBase = nomeBase;
	}

	public String getAtivo() {
		return ativo;
	}

	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ativo == null) ? 0 : ativo.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((itemMenuSECPai == null) ? 0 : itemMenuSECPai.hashCode());
		result = prime * result + nivel;
		result = prime * result
				+ ((nomeBase == null) ? 0 : nomeBase.hashCode());
		result = prime * result + ordem;
		result = prime * result + ((pagina == null) ? 0 : pagina.hashCode());
		result = prime * result
				+ ((subMenus == null) ? 0 : subMenus.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemMenuBean other = (ItemMenuBean) obj;
		if (ativo == null) {
			if (other.ativo != null)
				return false;
		} else if (!ativo.equals(other.ativo))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (itemMenuSECPai == null) {
			if (other.itemMenuSECPai != null)
				return false;
		} else if (!itemMenuSECPai.equals(other.itemMenuSECPai))
			return false;
		if (nivel != other.nivel)
			return false;
		if (nomeBase == null) {
			if (other.nomeBase != null)
				return false;
		} else if (!nomeBase.equals(other.nomeBase))
			return false;
		if (ordem != other.ordem)
			return false;
		if (pagina == null) {
			if (other.pagina != null)
				return false;
		} else if (!pagina.equals(other.pagina))
			return false;
		if (subMenus == null) {
			if (other.subMenus != null)
				return false;
		} else if (!subMenus.equals(other.subMenus))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ItemMenuBean [id=" + id + ", subMenus=" + subMenus
				+ ", itemMenuSECPai=" + itemMenuSECPai + ", nivel=" + nivel
				+ ", ordem=" + ordem + ", pagina=" + pagina + ", nomeBase="
				+ nomeBase + ", ativo=" + ativo + "]";
	}
	
}
