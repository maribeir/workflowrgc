package br.eng.rgc.beans;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Bean com atributos das Classificações.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */
@Entity
@Table(name = "RGC_CLASSIFICACAO")
public class ClassificacaoBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_CLASSIFICACAO",nullable=false)
	private Long id;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=UsuarioBean.class)
	@JoinColumn(name="ID_USUARIO",nullable=false)
	private UsuarioBean usuario;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=ItemBean.class)
	@JoinColumn(name="ID_ITEM",nullable=false)
	private ItemBean item;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=NCMBean.class)
	@JoinColumn(name="ID_NCM",nullable=false)
	private NCMBean ncm;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=DestaqueBean.class)
	@JoinColumn(name="ID_DESTAQUE",nullable=true)
	private DestaqueBean destaque;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=EXBean.class)
	@JoinColumn(name="ID_EX_II",nullable=true)
	private EXBean exii;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=EXBean.class)
	@JoinColumn(name="ID_EX_IPI",nullable=true)
	private EXBean exipi;
	
	@Column(name="S_DESCRICAO_LONGA",nullable=false)
	private String descricaoLonga;
	
	@Column(name="S_DESCRICAO_CURTA",nullable=true)
	private String descricaoCurta;
	
	@Column(name="S_DESCRICAO_CURTA_TRADUZIDA",nullable=true)
	private String descricaoCurtaTraduzida;
	
	@Column(name="S_STATUS",nullable=false)
	private String status;
	
	@Column(name="D_CADASTRO",nullable=false)
	private Date dataCadastro;
	
	@Column(name="S_OBSERVACAO",nullable=true)
	private String observacao;

    /**
     *  Construtor vazio.
     */
	public ClassificacaoBean(){
		this.id = new Long(0);
		this.usuario = new UsuarioBean();
		this.item = new ItemBean();
		this.ncm = new NCMBean();
		this.destaque = new DestaqueBean();
		this.exii = new EXBean();
		this.exipi = new EXBean();
		this.descricaoLonga = "";
		this.descricaoCurta = "";
		this.descricaoCurtaTraduzida = "";
		this.status = "";
		this.dataCadastro = new Date(0);
		this.observacao = "";
	}
    
	/**
	 *  Construtor com parâmetros.
	 *  
	 * @param id Id da Classificação.
	 * @param idUsuario Usuário responsável pela Classificação.
	 * @param idItem Item.
	 * @param idNCM NCM.
	 * @param idDestaque Destaque.
	 * @param idEXII EX de II.
	 * @param idEXIPI EX de IPI.
	 * @param descricaoLonga Descrição Longa da Classificação.
	 * @param descricaoCurta Descrição Curta da Classificação.
	 * @param descricaoCurtaTraduzida Descrição Curta Traduzida da Classificação.
	 * @param status Status da Classificação.
	 * @param dataCadastro Data de Cadastro da Classificação.
	 * @param observacao Observação.
	 */
	public ClassificacaoBean(Long id, UsuarioBean usuario, ItemBean item, NCMBean ncm, DestaqueBean destaque, EXBean exii, EXBean exipi,
			                 String descricaoLonga, String descricaoCurta, String descricaoCurtaTraduzida, String status,
			                 Date dataCadastro, String observacao) {
		this.id = id;
		this.usuario = usuario;	
		this.item = item;
		this.ncm = ncm;
		this.destaque = destaque;	
		this.exii = exii;	
		this.exipi = exipi;	
		this.descricaoLonga = descricaoLonga;
		this.descricaoCurta = descricaoCurta;
		this.descricaoCurtaTraduzida = descricaoCurtaTraduzida;
		this.status = status;
		this.dataCadastro = dataCadastro;
		this.observacao = observacao;
	}

	/**
	 * Recupera Id da Classificação.
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Define o Id da Classificação.
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Recupera Usuário responsável pela Classificação.
	 * @return usuario
	 */
	public UsuarioBean getUsuario() {
		return usuario;
	}
	/**
	 * Define o Usuário responsável pela Classificação.
	 * @param usuario
	 */
	public void setUsuario(UsuarioBean usuario) {
		this.usuario = usuario;
	}

	/**
	 * Recupera Item.
	 * @return item
	 */
	public ItemBean getItem() {
		return item;
	}
	/**
	 * Define o Item.
	 * @param item
	 */
	public void setItem(ItemBean item) {
		this.item = item;
	}

	/**
	 * Recupera NCM.
	 * @return ncm
	 */
	public NCMBean getNcm() {
		return ncm;
	}
	/**
	 * Define o NCM.
	 * @param ncm
	 */
	public void setNcm(NCMBean ncm) {
		this.ncm = ncm;
	}

	/**
	 * Recupera Destaque.
	 * @return destaque
	 */
	public DestaqueBean getDestaque() {
		return destaque;
	}
	/**
	 * Define o Destaque.
	 * @param destaque
	 */
	public void setDestaque(DestaqueBean destaque) {
		this.destaque = destaque;
	}

	/**
	 * Recupera EX de II.
	 * @return exii
	 */
	public EXBean getExii() {
		return exii;
	}
	/**
	 * Define o EX de II.
	 * @param exii
	 */
	public void setExii(EXBean exii) {
		this.exii = exii;
	}

	/**
	 * Recupera EX de IPI.
	 * @return exipi
	 */
	public EXBean getExipi() {
		return exipi;
	}
	/**
	 * Define o EX de IPI.
	 * @param exipi
	 */
	public void setExipi(EXBean exipi) {
		this.exipi = exipi;
	}

	/**
	 * Recupera Descrição Longa da Classificação.	
	 * @return descricaoLonga
	 */
	public String getDescricaoLonga() {
		return descricaoLonga;
	}
	/**
	 * Define a Descrição Longa da Classificação.
     * @param descricaoLonga
     */
	public void setDescricaoLonga(String descricaoLonga) {
		this.descricaoLonga = descricaoLonga;
	}

	/**
	 * Recupera Descrição Curta da Classificação.	
	 * @return descricaoCurta
	 */
	public String getDescricaoCurta() {
		return descricaoCurta;
	}
	/**
	 * Define a Descrição Curta da Classificação.
     * @param descricaoCurta
     */
	public void setDescricaoCurta(String descricaoCurta) {
		this.descricaoCurta = descricaoCurta;
	}

	/**
	 * Recupera Descrição Curta Traduzida da Classificação.	
	 * @return descricaoCurtaTraduzida
	 */
	public String getDescricaoCurtaTraduzida() {
		return descricaoCurtaTraduzida;
	}
	/**
	 * Define a Descrição Curta Traduzida da Classificação.
     * @param descricaoCurtaTraduzida
     */
	public void setDescricaoCurtaTraduzida(String descricaoCurtaTraduzida) {
		this.descricaoCurtaTraduzida = descricaoCurtaTraduzida;
	}

	/**
	 * Recupera Status da Classificação.	
	 * @return status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * Define o Status da Classificação.
     * @param status
     */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Recupera Data de Cadastro da Classificação.	
	 * @return dataCadastro
	 */
	public Date getDataCadastro() {
		return dataCadastro;
	}
	/**
	 * Define a Data de Cadastro da Classificação.
     * @param dataCadastro
     */
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	/**
	 * Recupera Observação.	
	 * @return observacao
	 */
	public String getObservacao() {
		return observacao;
	}
	/**
	 * Define a Observação.
     * @param status
     */
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dataCadastro == null) ? 0 : dataCadastro.hashCode());
		result = prime * result
				+ ((descricaoCurta == null) ? 0 : descricaoCurta.hashCode());
		result = prime
				* result
				+ ((descricaoCurtaTraduzida == null) ? 0
						: descricaoCurtaTraduzida.hashCode());
		result = prime * result
				+ ((descricaoLonga == null) ? 0 : descricaoLonga.hashCode());
		result = prime * result
				+ ((destaque == null) ? 0 : destaque.hashCode());
		result = prime * result + ((exii == null) ? 0 : exii.hashCode());
		result = prime * result + ((exipi == null) ? 0 : exipi.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((item == null) ? 0 : item.hashCode());
		result = prime * result + ((ncm == null) ? 0 : ncm.hashCode());
		result = prime * result
				+ ((observacao == null) ? 0 : observacao.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClassificacaoBean other = (ClassificacaoBean) obj;
		if (dataCadastro == null) {
			if (other.dataCadastro != null)
				return false;
		} else if (!dataCadastro.equals(other.dataCadastro))
			return false;
		if (descricaoCurta == null) {
			if (other.descricaoCurta != null)
				return false;
		} else if (!descricaoCurta.equals(other.descricaoCurta))
			return false;
		if (descricaoCurtaTraduzida == null) {
			if (other.descricaoCurtaTraduzida != null)
				return false;
		} else if (!descricaoCurtaTraduzida
				.equals(other.descricaoCurtaTraduzida))
			return false;
		if (descricaoLonga == null) {
			if (other.descricaoLonga != null)
				return false;
		} else if (!descricaoLonga.equals(other.descricaoLonga))
			return false;
		if (destaque == null) {
			if (other.destaque != null)
				return false;
		} else if (!destaque.equals(other.destaque))
			return false;
		if (exii == null) {
			if (other.exii != null)
				return false;
		} else if (!exii.equals(other.exii))
			return false;
		if (exipi == null) {
			if (other.exipi != null)
				return false;
		} else if (!exipi.equals(other.exipi))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (item == null) {
			if (other.item != null)
				return false;
		} else if (!item.equals(other.item))
			return false;
		if (ncm == null) {
			if (other.ncm != null)
				return false;
		} else if (!ncm.equals(other.ncm))
			return false;
		if (observacao == null) {
			if (other.observacao != null)
				return false;
		} else if (!observacao.equals(other.observacao))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ClassificacaoBean [id=" + id + ", usuario=" + usuario
				+ ", item=" + item + ", ncm=" + ncm + ", destaque=" + destaque
				+ ", exii=" + exii + ", exipi=" + exipi + ", descricaoLonga="
				+ descricaoLonga + ", descricaoCurta=" + descricaoCurta
				+ ", descricaoCurtaTraduzida=" + descricaoCurtaTraduzida
				+ ", status=" + status + ", dataCadastro=" + dataCadastro
				+ ", observacao=" + observacao + "]";
	}
}