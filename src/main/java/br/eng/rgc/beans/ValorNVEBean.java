package br.eng.rgc.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Bean com atributos dos Valores de NVE.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */
@Entity
@Table(name = "RGC_VALOR_NVE")
public class ValorNVEBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_VALOR_NVE",nullable=false)
	private Long id;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=AtributoNVEBean.class)
	@JoinColumn(name="ID_ATRIBUTO_NVE",nullable=false)
	private AtributoNVEBean atributoNVE;

	@Column(name="S_DESCRICAO",nullable=false)
	private String descricao;

	@Column(name="S_CODIGO_VALOR",nullable=false)
	private String codigoValor;

    /**
     *  Construtor vazio.
     */
	public ValorNVEBean(){
		this.id = new Long(0);
		this.atributoNVE = new AtributoNVEBean();
		this.descricao = "";
		this.codigoValor = "";
	}
    
	/**
	 *  Construtor com parâmetros.
	 *  
	 * @param id Id do Valor de NVE.
	 * @param atributoNVE Atributo de NVE.
	 * @param descricao Descrição do Valor de NVE.
	 * @param codigoValor Código do Valor de NVE.
	 */
	public ValorNVEBean(Long id, AtributoNVEBean atributoNVE, String descricao, String codigoValor) {
		this.id = id;
		this.atributoNVE = atributoNVE;
		this.descricao = descricao;
		this.codigoValor = codigoValor;
	}

	/**
	 * Recupera Id do Valor de NVE.
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Define o Id do Valor de NVE.
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Recupera Atributo de NVE.
	 * @return atributoNVE
	 */
	public AtributoNVEBean getAtributoNVE() {
		return atributoNVE;
	}
	/**
	 * Define o Atributo de NVE.
	 * @param atributoNVE
	 */
	public void setAtributoNVE(AtributoNVEBean atributoNVE) {
		this.atributoNVE = atributoNVE;
	}

	/**
	 * Recupera Descrição do Valor de NVE.	
	 * @return descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * Define a Descrição do Valor de NVE.	
     * @param descricao
     */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Recupera Código do Valor de NVE.	
	 * @return codigoValor
	 */
	public String getCodigoValor() {
		return codigoValor;
	}
	/**
	 * Define o Código do Valor de NVE.	
     * @param codigoValor
     */
	public void setCodigoValor(String codigoValor) {
		this.codigoValor = codigoValor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((atributoNVE == null) ? 0 : atributoNVE.hashCode());
		result = prime * result
				+ ((codigoValor == null) ? 0 : codigoValor.hashCode());
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValorNVEBean other = (ValorNVEBean) obj;
		if (atributoNVE == null) {
			if (other.atributoNVE != null)
				return false;
		} else if (!atributoNVE.equals(other.atributoNVE))
			return false;
		if (codigoValor == null) {
			if (other.codigoValor != null)
				return false;
		} else if (!codigoValor.equals(other.codigoValor))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ValorNVEBean [id=" + id + ", atributoNVE=" + atributoNVE
				+ ", descricao=" + descricao + ", codigoValor=" + codigoValor
				+ "]";
	}
}