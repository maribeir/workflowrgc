package br.eng.rgc.beans;

import java.sql.Connection;
import java.util.Locale;

/*
* @LoginBean.java
*
* Copyright (c) 2001-2016 Agrosoftware Ltda.
* Rua Conceição, 233 - Cj. 616 - Centro - Campinas - SP - Brasil
* Todos Direitos Reservados.
*/

/**
* Classe Bean referente ao Login
*
* @author   Matheus R. Torres
* @date     08/2017
* @version  1.0
*/
public class LoginBean extends BaseBean {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3566494282801230495L;
	
	// Atributos
	private Long        id;
	private UsuarioBean usuario;
	private Locale      locale;
	private boolean     senha;
	private boolean     loginPermitido;
    private final String codigoIdioma = "pt";
    private final String codigoPais   = "BR";
    
	private transient   Connection  conexao;

    /**
    * Construtor com parâmetros
    */
	public LoginBean(Long id, UsuarioBean usuario, Locale locale, boolean senha,
			         boolean loginPermitido, Connection conexao) {
		this.id = id;
		this.usuario = usuario;
		this.locale = locale;
		this.senha = senha;
		this.loginPermitido = loginPermitido;
		this.conexao = conexao;
	}

    /**
    * Construtor sem parâmetros
    */
	public LoginBean() {
		this.id = new Long(0);
		this.usuario = new UsuarioBean();
		this.locale = new Locale(codigoIdioma, codigoPais);
		this.senha = false;
		this.loginPermitido = false;
		this.conexao = null;
	}


    /**
    * Getters e Setters
    */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public UsuarioBean getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioBean usuario) {
		this.usuario = usuario;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public boolean isSenha() {
		return senha;
	}

	public void setSenha(boolean senha) {
		this.senha = senha;
	}

	public boolean isLoginPermitido() {
		return loginPermitido;
	}

	public void setLoginPermitido(boolean loginPermitido) {
		this.loginPermitido = loginPermitido;
	}

	public Connection getConexao() {
		return conexao;
	}

	public void setConexao(Connection conexao) {
		this.conexao = conexao;
	}

	public String getCodigoIdioma() {
		return codigoIdioma;
	}

	public String getCodigoPais() {
		return codigoPais;
	}

	@Override
	public String toString() {
		return "LoginBean [id=" + id + ", usuario=" + usuario + ", locale=" + locale + ", senha=" + senha
				+ ", loginPermitido=" + loginPermitido + ", codigoIdioma="
				+ codigoIdioma + ", codigoPais=" + codigoPais + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((codigoIdioma == null) ? 0 : codigoIdioma.hashCode());
		result = prime * result
				+ ((codigoPais == null) ? 0 : codigoPais.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((locale == null) ? 0 : locale.hashCode());
		result = prime * result + (loginPermitido ? 1231 : 1237);
		result = prime * result + (senha ? 1231 : 1237);
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoginBean other = (LoginBean) obj;
		if (codigoIdioma == null) {
			if (other.codigoIdioma != null)
				return false;
		} else if (!codigoIdioma.equals(other.codigoIdioma))
			return false;
		if (codigoPais == null) {
			if (other.codigoPais != null)
				return false;
		} else if (!codigoPais.equals(other.codigoPais))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (locale == null) {
			if (other.locale != null)
				return false;
		} else if (!locale.equals(other.locale))
			return false;
		if (loginPermitido != other.loginPermitido)
			return false;
		if (senha != other.senha)
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}
}
