package br.eng.rgc.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Bean com atributos dos Departamentos.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 07/2017
 * 
 */
@Entity
@Table(name = "RGC_DEPARTAMENTO")
public class DepartamentoBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_DEPARTAMENTO",nullable=false)
	private Long id;

	@Column(name="S_DESCRICAO",nullable=false)
	private String descricao;

    /**
     *  Construtor vazio.
     */
	public DepartamentoBean(){
		this.id = new Long(0);
		this.descricao = "";
	}
    
	/**
	 *  Construtor com parâmetros.
	 *  
	 * @param id Id do Departamento.
	 * @param descricao Descrição do Departamento.
	 */
	public DepartamentoBean(Long id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	/**
	 * Recupera Id do Departamento.
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Define o Id do Departamento.
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Recupera Descrição do Departamento.	
	 * @return descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * Define a Descrição do Departamento.	
     * @param descricao
     */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DepartamentoBean other = (DepartamentoBean) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DepartamentoBean [id=" + id + ", descricao=" + descricao + "]";
	}	
}