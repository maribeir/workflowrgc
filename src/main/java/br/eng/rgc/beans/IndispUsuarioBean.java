package br.eng.rgc.beans;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Bean com atributos das Indisponibilidades de Usuários.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */
@Entity
@Table(name = "RGC_INDISP_USUARIO")
public class IndispUsuarioBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_INDISP_USUARIO",nullable=false)
	private Long id;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=UsuarioBean.class)
	@JoinColumn(name="ID_USUARIO",nullable=false)
	private UsuarioBean usuario;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=UsuarioBean.class)
	@JoinColumn(name="ID_USUARIO_SUBSTITUTO",nullable=true)
	private UsuarioBean usuarioSubstituto;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=UsuarioBean.class)
	@JoinColumn(name="ID_USUARIO_APROVADOR",nullable=false)
	private UsuarioBean usuarioAprovador;

	@Column(name="D_INICIO",nullable=false)
	private Date dataInicio;

	@Column(name="D_FIM",nullable=true)
	private Date dataFim;

	@Column(name="S_JUSTIFICATIVA",nullable=true)
	private String justificativa;

    /**
     *  Construtor vazio.
     */
	public IndispUsuarioBean(){
		this.id = new Long(0);
		this.usuario = new UsuarioBean();
		this.usuarioSubstituto = new UsuarioBean();
		this.usuarioAprovador = new UsuarioBean();
		this.dataInicio = new Date(0);
		this.dataFim = new Date(0);
		this.justificativa = "";
	}
    
	/**
	 *  Construtor com parâmetros.
	 *  
	 * @param id Id de Indisponibilidade do Usuário.
	 * @param usuario Usuário Indisponível.
	 * @param usuarioSubstituto Usuário Substituto.
	 * @param usuarioAprovador Usuário Aprovador da Indisponibilidade.
	 * @param dataInicio Data de Início da Indisponibilidade do Usuário.
	 * @param dataFim Data de Fim da Indisponibilidade do Usuário.
	 * @param justificativa Justificativa da Indisponibilidade do Usuário.
	 */
	public IndispUsuarioBean(Long id, UsuarioBean usuario, UsuarioBean usuarioSubstituto, UsuarioBean usuarioAprovador,
			                 Date dataInicio, Date dataFim, String justificativa) {
		this.id = id;
		this.usuario = usuario;
		this.usuarioSubstituto = usuarioSubstituto;
		this.usuarioAprovador = usuarioAprovador;
		this.dataInicio = dataInicio;
		this.dataFim = dataFim;
		this.justificativa = justificativa;
	}

	/**
	 * Recupera Id de Indisponibilidade do Usuário.
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Define o Id de Indisponibilidade do Usuário.
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Recupera Usuário Indisponível.
	 * @return usuario
	 */
	public UsuarioBean getUsuario() {
		return usuario;
	}
	/**
	 * Define o Usuário Indisponível.
	 * @param usuario
	 */
	public void setUsuario(UsuarioBean usuario) {
		this.usuario = usuario;
	}

	/**
	 * Recupera Usuário Substituto.
	 * @return usuarioSubstituto
	 */
	public UsuarioBean getUsuarioSubstituto() {
		return usuarioSubstituto;
	}
	/**
	 * Define o Usuário Substituto.
	 * @param usuarioSubstituto
	 */
	public void setUsuarioSubstituto(UsuarioBean usuarioSubstituto) {
		this.usuarioSubstituto = usuarioSubstituto;
	}

	/**
	 * Recupera Usuário Aprovador da Indisponibilidade.
	 * @return usuarioAprovador
	 */
	public UsuarioBean getUsuarioAprovador() {
		return usuarioAprovador;
	}
	/**
	 * Define o Usuário Aprovador da Indisponibilidade.
	 * @param idUsuarioAprovador
	 */
	public void setUsuarioAprovador(UsuarioBean usuarioAprovador) {
		this.usuarioAprovador = usuarioAprovador;
	}

	/**
	 * Recupera Data de Início da Indisponibilidade do Usuário.	
	 * @return dataInicio
	 */
	public Date getDataInicio() {
		return dataInicio;
	}
	/**
	 * Define a Data de Início da Indisponibilidade do Usuário.	
     * @param descricao
     */
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	/**
	 * Recupera Data de Fim da Indisponibilidade do Usuário.	
	 * @return dataFim
	 */
	public Date getDataFim() {
		return dataFim;
	}
	/**
	 * Define a Data de Fim da Indisponibilidade do Usuário.	
     * @param descricao
     */
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	/**
	 * Recupera Justificativa da Indisponibilidade do Usuário.	
	 * @return justificativa
	 */
	public String getJustificativa() {
		return justificativa;
	}
	/**
	 * Define a Justificativa da Indisponibilidade do Usuário.	
     * @param justificativa
     */
	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataFim == null) ? 0 : dataFim.hashCode());
		result = prime * result
				+ ((dataInicio == null) ? 0 : dataInicio.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((justificativa == null) ? 0 : justificativa.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		result = prime
				* result
				+ ((usuarioAprovador == null) ? 0 : usuarioAprovador.hashCode());
		result = prime
				* result
				+ ((usuarioSubstituto == null) ? 0 : usuarioSubstituto
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IndispUsuarioBean other = (IndispUsuarioBean) obj;
		if (dataFim == null) {
			if (other.dataFim != null)
				return false;
		} else if (!dataFim.equals(other.dataFim))
			return false;
		if (dataInicio == null) {
			if (other.dataInicio != null)
				return false;
		} else if (!dataInicio.equals(other.dataInicio))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (justificativa == null) {
			if (other.justificativa != null)
				return false;
		} else if (!justificativa.equals(other.justificativa))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		if (usuarioAprovador == null) {
			if (other.usuarioAprovador != null)
				return false;
		} else if (!usuarioAprovador.equals(other.usuarioAprovador))
			return false;
		if (usuarioSubstituto == null) {
			if (other.usuarioSubstituto != null)
				return false;
		} else if (!usuarioSubstituto.equals(other.usuarioSubstituto))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "IndispUsuarioBean [id=" + id + ", usuario=" + usuario
				+ ", usuarioSubstituto=" + usuarioSubstituto
				+ ", usuarioAprovador=" + usuarioAprovador + ", dataInicio="
				+ dataInicio + ", dataFim=" + dataFim + ", justificativa="
				+ justificativa + "]";
	}
}