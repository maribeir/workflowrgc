package br.eng.rgc.beans;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Bean com atributos dos Contratos com os Clientes.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */
@Entity
@Table(name = "RGC_CLIENTE_CONTRATO")
public class ClienteContratoBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_CLIENTE_CONTRATO",nullable=false)
	private Long id;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=ClienteBean.class)
	@JoinColumn(name="ID_CLIENTE",nullable=false)
	private ClienteBean cliente;

	@Column(name="S_TIPO_CONTRATO",nullable=false)
	private String tipoContrato;

	@Column(name="N_QUANTIDADE_ITEM",nullable=true)
	private Integer quantidadeItem;

	@Column(name="N_QUANTIDADE_HORA",nullable=true)
	private Integer quantidadeHora;

	@Column(name="D_VIGENCIA",nullable=false)
	private Date dataVigencia;

	@Column(name="S_RECORRENTE",nullable=false)
	private String recorrente;

	@Column(name="S_PERIODICIDADE_RECORRENCIA",nullable=true)
	private String periodicidadeRecorrencia;

	@Column(name="N_UNIDADE_RECORRENCIA",nullable=true)
	private Integer unidadeRecorrencia;

    /**
     *  Construtor vazio.
     */
	public ClienteContratoBean(){
		this.id = new Long(0);
		this.cliente = new ClienteBean();
		this.tipoContrato = "";
		this.quantidadeItem = 0;
		this.quantidadeHora = 0;
		this.dataVigencia = new Date(0);
		this.recorrente = "";
		this.periodicidadeRecorrencia = "";
		this.unidadeRecorrencia = 0;
	}
    
	/**
	 *  Construtor com parâmetros.
	 *  
	 * @param id Id do Contrato com o Cliente.
	 * @param cliente Cliente.
	 * @param tipoContrato Tipo de Contrato com o Cliente.
	 * @param quantidadeItem Quantidade de Itens no Contrato com o Cliente.
	 * @param quantidadeHora Quantidade de Horas no Contrato com o Cliente.
	 * @param dataVigencia Data de Vigência do Contrato com o Cliente.
	 * @param recorrente Flag Recorrente (S ou N).
	 * @param periodicidadeRecorrencia Periodicidade de Recorrência do Contrato com o Cliente.
	 * @param unidadeRecorrencia Unidade de Recorrência do Contrato com o Cliente.
	 */
	public ClienteContratoBean(Long id, ClienteBean cliente, String tipoContrato, Integer quantidadeItem, Integer quantidadeHora,
			                   Date dataVigencia, String recorrente, String periodicidadeRecorrencia, Integer unidadeRecorrencia) {
		this.id = id;
		this.cliente = cliente;
		this.tipoContrato = tipoContrato;
		this.quantidadeItem = quantidadeItem;
		this.quantidadeHora = quantidadeHora;
		this.dataVigencia = dataVigencia;
		this.recorrente = recorrente;
		this.periodicidadeRecorrencia = periodicidadeRecorrencia;
		this.unidadeRecorrencia = unidadeRecorrencia;
	}

	/**
	 * Recupera Id do Contrato com o Cliente.
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Define o Id do Contrato com o Cliente.
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Recupera Cliente.
	 * @return cliente
	 */
	public ClienteBean getIdCliente() {
		return cliente;
	}
	/**
	 * Define o Cliente.
	 * @param cliente
	 */
	public void setIdCliente(ClienteBean cliente) {
		this.cliente = cliente;
	}

	/**
	 * Recupera Tipo do Contato do Cliente.	
	 * @return tipoContrato
	 */
	public String getTipoContrato() {
		return tipoContrato;
	}
	/**
	 * Define o Tipo do Contato do Cliente.	
     * @param tipoContrato
     */
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	/**
	 * Recupera Quantidade de Itens no Contrato com o Cliente.	
	 * @return quantidadeItem
	 */
	public Integer getQuantidadeItem() {
		return quantidadeItem;
	}
	/**
	 * Define a Quantidade de Itens no Contrato com o Cliente.	
     * @param quantidadeItem
     */
	public void setQuantidadeItem(Integer quantidadeItem) {
		this.quantidadeItem = quantidadeItem;
	}

	/**
	 * Recupera Quantidade de Horas no Contrato com o Cliente.	
	 * @return quantidadeHora
	 */
	public Integer getQuantidadeHora() {
		return quantidadeHora;
	}
	/**
	 * Define a Quantidade de Horas no Contrato com o Cliente.	
     * @param quantidadeHora
     */
	public void setQuantidadeHora(Integer quantidadeHora) {
		this.quantidadeHora = quantidadeHora;
	}

	/**
	 * Recupera Data de Vigência do Contrato com o Cliente.	
	 * @return quantidadeHora
	 */
	public Date getDataVigencia() {
		return dataVigencia;
	}
	/**
	 * Define a Data de Vigência do Contrato com o Cliente.	
     * @param dataVigencia
     */
	public void setDataVigencia(Date dataVigencia) {
		this.dataVigencia = dataVigencia;
	}

	/**
	 * Recupera Flag Recorrente (S ou N).	
	 * @return recorrente
	 */
	public String getRecorrente() {
		return recorrente;
	}
	/**
	 * Define a Flag Recorrente (S ou N).	
     * @param recorrente
     */
	public void setRecorrente(String recorrente) {
		this.recorrente = recorrente;
	}

	/**
	 * Recupera Periodicidade de Recorrência do Contrato com o Cliente.	
	 * @return periodicidadeRecorrencia
	 */
	public String getPeriodicidadeRecorrencia() {
		return periodicidadeRecorrencia;
	}
	/**
	 * Define a Periodicidade de Recorrência do Contrato com o Cliente.	
     * @param periodicidadeRecorrencia
     */
	public void setPeriodicidadeRecorrencia(String periodicidadeRecorrencia) {
		this.periodicidadeRecorrencia = periodicidadeRecorrencia;
	}

	/**
	 * Recupera Unidade de Recorrência do Contrato com o Cliente.	
	 * @return unidadeRecorrencia
	 */
	public Integer getUnidadeRecorrencia() {
		return unidadeRecorrencia;
	}
	/**
	 * Define a Unidade de Recorrência do Contrato com o Cliente.	
     * @param unidadeRecorrencia
     */
	public void setUnidadeRecorrencia(Integer unidadeRecorrencia) {
		this.unidadeRecorrencia = unidadeRecorrencia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cliente == null) ? 0 : cliente.hashCode());
		result = prime * result
				+ ((dataVigencia == null) ? 0 : dataVigencia.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((periodicidadeRecorrencia == null) ? 0
						: periodicidadeRecorrencia.hashCode());
		result = prime * result
				+ ((quantidadeHora == null) ? 0 : quantidadeHora.hashCode());
		result = prime * result
				+ ((quantidadeItem == null) ? 0 : quantidadeItem.hashCode());
		result = prime * result
				+ ((recorrente == null) ? 0 : recorrente.hashCode());
		result = prime * result
				+ ((tipoContrato == null) ? 0 : tipoContrato.hashCode());
		result = prime
				* result
				+ ((unidadeRecorrencia == null) ? 0 : unidadeRecorrencia
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClienteContratoBean other = (ClienteContratoBean) obj;
		if (cliente == null) {
			if (other.cliente != null)
				return false;
		} else if (!cliente.equals(other.cliente))
			return false;
		if (dataVigencia == null) {
			if (other.dataVigencia != null)
				return false;
		} else if (!dataVigencia.equals(other.dataVigencia))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (periodicidadeRecorrencia == null) {
			if (other.periodicidadeRecorrencia != null)
				return false;
		} else if (!periodicidadeRecorrencia
				.equals(other.periodicidadeRecorrencia))
			return false;
		if (quantidadeHora == null) {
			if (other.quantidadeHora != null)
				return false;
		} else if (!quantidadeHora.equals(other.quantidadeHora))
			return false;
		if (quantidadeItem == null) {
			if (other.quantidadeItem != null)
				return false;
		} else if (!quantidadeItem.equals(other.quantidadeItem))
			return false;
		if (recorrente == null) {
			if (other.recorrente != null)
				return false;
		} else if (!recorrente.equals(other.recorrente))
			return false;
		if (tipoContrato == null) {
			if (other.tipoContrato != null)
				return false;
		} else if (!tipoContrato.equals(other.tipoContrato))
			return false;
		if (unidadeRecorrencia == null) {
			if (other.unidadeRecorrencia != null)
				return false;
		} else if (!unidadeRecorrencia.equals(other.unidadeRecorrencia))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ClienteContratoBean [id=" + id + ", cliente=" + cliente
				+ ", tipoContrato=" + tipoContrato + ", quantidadeItem="
				+ quantidadeItem + ", quantidadeHora=" + quantidadeHora
				+ ", dataVigencia=" + dataVigencia + ", recorrente="
				+ recorrente + ", periodicidadeRecorrencia="
				+ periodicidadeRecorrencia + ", unidadeRecorrencia="
				+ unidadeRecorrencia + "]";
	}
}