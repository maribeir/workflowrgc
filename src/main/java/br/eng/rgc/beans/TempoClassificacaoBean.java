package br.eng.rgc.beans;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Bean com atributos dos Tempos de Classificação.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */
@Entity
@Table(name = "RGC_TEMPO_CLASSIFICACAO")
public class TempoClassificacaoBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_TEMPO_CLASSIFICACAO",nullable=false)
	private Long id;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=ClassificacaoBean.class)
	@JoinColumn(name="ID_CLASSIFICACAO",nullable=false)
	private ClassificacaoBean classificacao;

	@Column(name="S_TIPO",nullable=false)
	private String tipo;

	@Column(name="D_TEMPO",nullable=false)
	private Date tempo;

	@Column(name="S_OBSERVACAO",nullable=false)
	private String observacao;

    /**
     *  Construtor vazio.
     */
	public TempoClassificacaoBean(){
		this.id = new Long(0);
		this.classificacao = new ClassificacaoBean();
		this.tipo = "";
		this.tempo = new Date(0);
		this.observacao = "";
	}
    
	/**
	 *  Construtor com parâmetros.
	 *  
	 * @param id Id do registro de horário de uma operação durante uma classificação.
	 * @param classificacao Classificação.
	 * @param tipo Tipo do registro de horário.
	 * @param tempo Horário da operação.
	 * @param observacao Observação.
	 */
	public TempoClassificacaoBean(Long id, ClassificacaoBean classificacao, String tipo, Date tempo, String observacao) {
		this.id = id;
		this.classificacao = classificacao;
		this.tipo = tipo;
	}

	/**
	 * Recupera Id do registro de horário de uma operação durante uma classificação.
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Define o Id do registro de horário de uma operação durante uma classificação.
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Recupera Classificação.
	 * @return classificacao
	 */
	public ClassificacaoBean getClassificacao() {
		return classificacao;
	}
	/**
	 * Define o Classificação.
	 * @param classificacao
	 */
	public void setIdClassificacao(ClassificacaoBean classificacao) {
		this.classificacao = classificacao;
	}

	/**
	 * Recupera Tipo do registro de horário.	
	 * @return tipo
	 */
	public String getTipo() {
		return tipo;
	}
	/**
	 * Define o Tipo do registro de horário.	
     * @param tipo
     */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Recupera Horário da operação.	
	 * @return tempo
	 */
	public Date getTempo() {
		return tempo;
	}
	/**
	 * Define o Horário da operação.	
     * @param tempo
     */
	public void setTempo(Date tempo) {
		this.tempo = tempo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((classificacao == null) ? 0 : classificacao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((observacao == null) ? 0 : observacao.hashCode());
		result = prime * result + ((tempo == null) ? 0 : tempo.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TempoClassificacaoBean other = (TempoClassificacaoBean) obj;
		if (classificacao == null) {
			if (other.classificacao != null)
				return false;
		} else if (!classificacao.equals(other.classificacao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (observacao == null) {
			if (other.observacao != null)
				return false;
		} else if (!observacao.equals(other.observacao))
			return false;
		if (tempo == null) {
			if (other.tempo != null)
				return false;
		} else if (!tempo.equals(other.tempo))
			return false;
		if (tipo == null) {
			if (other.tipo != null)
				return false;
		} else if (!tipo.equals(other.tipo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TempoClassificacaoBean [id=" + id + ", classificacao="
				+ classificacao + ", tipo=" + tipo + ", tempo=" + tempo
				+ ", observacao=" + observacao + "]";
	}
}