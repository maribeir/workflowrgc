package br.eng.rgc.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Bean com atributos dos Atributos de NVE.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */
@Entity
@Table(name = "RGC_ATRIBUTO_NVE")
public class AtributoNVEBean extends BaseBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_ATRIBUTO_NVE",nullable=false)
	private Long id;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=NCMBean.class)
    @JoinColumn(name="ID_NCM",nullable=false)
	private NCMBean ncm;
	
	@Column(name="S_DESCRICAO",nullable=false)
	private String descricao;

	@Column(name="S_CODIGO_ATRIBUTO",nullable=false)
	private String codigoAtributo;

    /**
     *  Construtor vazio.
     */
	public AtributoNVEBean(){
		this.id = new Long(0);
		this.ncm = new NCMBean();
		this.descricao = "";
		this.codigoAtributo = "";
	}
    
	/**
	 *  Construtor com parâmetros.
	 *  
	 * @param id Id do Atributo de NVE.
	 * @param ncm NCM.
	 * @param descricao Descrição do Atributo de NVE.
	 * @param codigoAtributo Código do Atributo de NVE.
	 */
	public AtributoNVEBean(Long id, NCMBean ncm, String descricao, String codigoAtributo) {
		this.id = id;
		this.ncm = ncm;
		this.descricao = descricao;
		this.codigoAtributo = codigoAtributo;
	}

	/**
	 * Recupera Id do Atributo de NVE.
	 * @return id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * Define o Id do Atributo de NVE.
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Recupera NCM.
	 * @return ncm
	 */
	public NCMBean getNcm() {
		return ncm;
	}
	/**
	 * Define o NCM.
	 * @param idNCM
	 */
	public void setNcm(NCMBean ncm) {
		this.ncm = ncm;
	}

	/**
	 * Recupera Descrição do Atributo de NVE.	
	 * @return descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * Define a Descrição do Atributo de NVE.	
     * @param descricao
     */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Recupera Descrição do Atributo de NVE.	
	 * @return codigoAtributo
	 */
	public String getCodigoAtributo() {
		return codigoAtributo;
	}
	/**
	 * Define o Código do Atributo de NVE.	
     * @param codigoAtributo
     */
	public void setCodigoAtributo(String codigoAtributo) {
		this.codigoAtributo = codigoAtributo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codigoAtributo == null) ? 0 : codigoAtributo.hashCode());
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ncm == null) ? 0 : ncm.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AtributoNVEBean other = (AtributoNVEBean) obj;
		if (codigoAtributo == null) {
			if (other.codigoAtributo != null)
				return false;
		} else if (!codigoAtributo.equals(other.codigoAtributo))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ncm == null) {
			if (other.ncm != null)
				return false;
		} else if (!ncm.equals(other.ncm))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AtributoNVEBean [id=" + id + ", ncm=" + ncm + ", descricao="
				+ descricao + ", codigoAtributo=" + codigoAtributo + "]";
	}
}