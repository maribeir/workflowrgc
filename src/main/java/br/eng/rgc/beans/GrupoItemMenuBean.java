package br.eng.rgc.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Bean com atributos dos vínculos entre Grupos e Itens de Menu.
 * 
 * @author Matheus R. Torres
 * @version 1.0.0
 * @date 08/2017
 * 
 */
@Entity
@Table(name = "SEC_GRUPO_ITEM_MENU")
public class GrupoItemMenuBean {

	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ID_GRUPO_ITEM_MENU",nullable=false)
	private Long id;	

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=GrupoBean.class)
	@JoinColumn(name="ID_GRUPO",nullable=false)
	private GrupoBean grupo;

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=ItemMenuBean.class)
	@JoinColumn(name="ID_ITEM_MENU",nullable=false)
	private ItemMenuBean itemMenu;
	
	public GrupoItemMenuBean()
	{
		this.id = new Long(0);
		this.grupo = new GrupoBean();
		this.itemMenu = new ItemMenuBean();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public GrupoBean getGrupo() {
		return grupo;
	}

	public void setGrupo(GrupoBean grupo) {
		this.grupo = grupo;
	}

	public ItemMenuBean getItemMenu() {
		return itemMenu;
	}

	public void setItemMenu(ItemMenuBean itemMenu) {
		this.itemMenu = itemMenu;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((grupo == null) ? 0 : grupo.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((itemMenu == null) ? 0 : itemMenu.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrupoItemMenuBean other = (GrupoItemMenuBean) obj;
		if (grupo == null) {
			if (other.grupo != null)
				return false;
		} else if (!grupo.equals(other.grupo))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (itemMenu == null) {
			if (other.itemMenu != null)
				return false;
		} else if (!itemMenu.equals(other.itemMenu))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "GrupoItemMenuBean [id=" + id + ", grupo=" + grupo
				+ ", itemMenu=" + itemMenu + "]";
	}
	
}
