package br.eng.rgc.managedbeans;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import br.eng.rgc.beans.CargoBean;
import br.eng.rgc.beans.ClassificacaoBean;
import br.eng.rgc.beans.IndispUsuarioBean;
import br.eng.rgc.beans.LogBean;
import br.eng.rgc.beans.LoginBean;
import br.eng.rgc.beans.UsuarioBean;
import br.eng.rgc.beans.UsuarioClienteBean;
import br.eng.rgc.beans.UsuarioDepartamentoBean;
import br.eng.rgc.repositories.UsuarioRepository;
import br.eng.rgc.util.WorkflowRGC;

/***
 * @author Matheus R. Torres
 * @version 1.0
 * @Date 08/2017
 *
 * ManagedBean para controlar o Login.
 */
@ManagedBean 
@ViewScoped
public class LoginMB implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private UsuarioRepository repository;

	private String username;

	private String password = "";
	
	@PostConstruct
	public void init(){	
		username = "";
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}public LoginMB() {
		efetuarLogout();				
	}

	/**
	 * Método que executa para remover o usuario da sessão quando efetuar o logout
	 */
	public void efetuarLogout() {
		HttpSession sessao = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		sessao.removeAttribute(WorkflowRGC.LOGIN_USUARIO);
	}


	/**
	 * Método que efetua o login do usuário
	 */
	public void efetuaLogin(){
		FacesMessage msg = null;
		System.out.println("");
		System.out.println("  -- INICIO --");
		System.out.println("");
		try{

			System.out.println("");
			System.out.println("  -- ENTROU TRY --");
			System.out.println("  .repository: " + repository);
			System.out.println("");
			
			//UsuarioBean usuario = repository.findOne(new Long(1));
			UsuarioBean usuario = new UsuarioBean(new Long(2), new CargoBean(new Long(1), "Gerente"), "Matheus", "32165498702", "matheus@matheus.com", "matheus", "3232-4040", "teste", "S", "S", new Date(new java.util.Date().getTime()), new ArrayList<ClassificacaoBean>(), new ArrayList<IndispUsuarioBean>(), new ArrayList<IndispUsuarioBean>(), new ArrayList<IndispUsuarioBean>(), new ArrayList<LogBean>(), new ArrayList<UsuarioClienteBean>(), new ArrayList<UsuarioDepartamentoBean>());
			
			if (username.equals(usuario.getUsername()) && password.equals(usuario.getSenha())){
				System.out.println("");
				System.out.println("  -- LOGIN OK --");
				System.out.println("");
				//Usuario pode acessar o sistema
				HttpSession sessao = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
				LoginBean login = new LoginBean();
				login.setUsuario(usuario);
				sessao.setAttribute(WorkflowRGC.LOGIN_USUARIO, login);
				FacesContext.getCurrentInstance().getExternalContext().redirect("exemplo.xhtml");
			}
			else
			{
				System.out.println("");
				System.out.println("  -- USUARIO OU SENHA ERRADA --");
				System.out.println("  Username: " + usuario.getUsername());
				System.out.println("  Senha: " + usuario.getSenha());
				System.out.println("");
				//Situação em que o usuário errou a senha, não é cadasrado ou foi boqueado.
				//Nesses casos será direcionado novamente para a tela de LOGIN.
				//Errou a Senha
				msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Usuário ou senha inválido(s).", "Usuário ou senha inválido(s).");
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}