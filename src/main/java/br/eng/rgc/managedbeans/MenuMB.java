package br.eng.rgc.managedbeans;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;
import org.springframework.beans.factory.annotation.Autowired;

import br.eng.rgc.beans.ItemMenuBean;
import br.eng.rgc.beans.LoginBean;
import br.eng.rgc.beans.UsuarioBean;
import br.eng.rgc.repositories.ItemMenuRepository;
import br.eng.rgc.util.WorkflowRGC;

/**
 * Managed Bean responsável por controlar a montagem do menu
 * 
 * @author Fernando Barros
 * @version 1.0
 * @date 11/2015
 */

@ManagedBean
@ViewScoped
public class MenuMB implements Serializable  {

	private static final long serialVersionUID = -9094817930460279294L;
	
	private static final Logger log = Logger.getLogger(MenuMB.class);

	private MenuModel menuModel;
	
	@Autowired
	private ItemMenuRepository repository;

	public void gerarMenu(){
		try{ 	
			//recuperando o usuário na sessao
			HttpSession sessao = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
			LoginBean loginUsuario = (LoginBean) sessao.getAttribute(WorkflowRGC.LOGIN_USUARIO);
			menuModel = new DefaultMenuModel();			
			
			/*List<ItemMenuBean> listaMenu = repository.findByUsername(loginUsuario.getUsuario().getUsername());
			for (ItemMenuBean menu : listaMenu) 
			{	
				DefaultSubMenu submenu = new DefaultSubMenu();
				submenu.setLabel(menu.getNomeBase());						
				for(ItemMenuBean mMenu : menu.getSubMenus()){					
					if(mMenu.getSubMenus().size() == 0){				
						DefaultMenuItem mi = new DefaultMenuItem();						
						mi.setValue(mMenu.getNomeBase());
						mi.setUrl(mMenu.getPagina());  						
						submenu.addElement(mi);
					}else{
						submenu.addElement(geraSubmenu(mMenu,loginUsuario.getLocale()));		
					}
				}
				menuModel.addElement(submenu);
			}*/
			
			DefaultSubMenu submenu = new DefaultSubMenu();
			submenu.setLabel("Cadastro");	
			DefaultMenuItem mi = new DefaultMenuItem();						
			mi.setValue("Usuário");
			mi.setUrl("cadastro_usuario.xhtml");  						
			submenu.addElement(mi);
			DefaultMenuItem mi2 = new DefaultMenuItem();						
			mi2.setValue("Item");
			mi2.setUrl("cadastro_item.xhtml");  						
			submenu.addElement(mi2);
			menuModel.addElement(submenu);
		}catch(Exception e){
			log.error("Erro ao gerar o menu : ",e);	
			e.printStackTrace();			
		}
	}
	
	public DefaultSubMenu geraSubmenu(ItemMenuBean menu,Locale locale){
		DefaultSubMenu submenu = new DefaultSubMenu();
		try{
			submenu.setLabel(menu.getNomeBase());		  	  
			for (ItemMenuBean mSubMenu : menu.getSubMenus()){
				DefaultMenuItem mi = new DefaultMenuItem();
				mi.setValue(mSubMenu.getNomeBase());				 
				mi.setUrl(mSubMenu.getPagina());
				submenu.addElement(mi);
			}
		}catch(Exception e ){
			System.out.println("MenuMB.geraSubmenu() - ERRO:  " +e.getMessage());  
		}
		return submenu;
	}
	
	
	public MenuModel getMenuModel(){
		return  menuModel;
    }

	public MenuMB(){
		log.info("Call gerarMenu()");
		gerarMenu();
	}

}