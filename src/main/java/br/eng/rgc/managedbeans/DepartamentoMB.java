package br.eng.rgc.managedbeans;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import br.eng.rgc.repositories.DepartamentoRepository;

/**
 * @author Matheus R. Torres
 * @version 1.0.0
 * @Date 07/2017
 *
 * ManagedBean para controlar o cadastro de departamentos
 */
@ManagedBean
@ViewScoped
public class DepartamentoMB {

	@Autowired
	private DepartamentoRepository repository;
	
	public DepartamentoMB(){}
	
	@PostConstruct
	public void init(){
		//recuperando o usuário na sessao
		HttpSession sessao = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		sessao.getAttribute("LOGIN");
	}
	
}
