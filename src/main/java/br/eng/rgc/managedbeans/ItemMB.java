package br.eng.rgc.managedbeans;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;

import br.eng.rgc.beans.ClassificacaoBean;
import br.eng.rgc.beans.ItemCatalogoBean;
import br.eng.rgc.beans.LoginBean;
import br.eng.rgc.beans.ItemBean;
import br.eng.rgc.beans.ClienteBean;
import br.eng.rgc.beans.DepartamentoBean;
import br.eng.rgc.beans.NCMBean;
import br.eng.rgc.repositories.ItemRepository;
import br.eng.rgc.repositories.ClienteRepository;
import br.eng.rgc.repositories.DepartamentoRepository;
import br.eng.rgc.repositories.NCMRepository;
import br.eng.rgc.util.MessagesView;
import br.eng.rgc.util.UlResourceBundle;
import br.eng.rgc.util.WorkflowRGC;

/***
 * @author Fernando L. de Barros
 * @version 1.0
 * @Date 08/2017
 *
 * ManagedBean para controlar o Cadastro de Item.
 */
@ManagedBean
@ViewScoped
public class ItemMB implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8875318859508778140L;

	@Autowired
	private ItemRepository repository;
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private DepartamentoRepository departamentoRepository;
	
	@Autowired
	private NCMRepository ncmRepository;
	
	private UlResourceBundle resource = new UlResourceBundle("item");
	private List<ItemBean> itens;
	private List<ClienteBean> clientes;
	private List<DepartamentoBean> departamentos;
	private List<NCMBean> ncms;
	private ItemBean item;
	private ItemBean filtro;
	private LoginBean login;
	
	public ItemMB(){}
	
	@PostConstruct
	public void init(){
		// Recuperando o usuário na sessão
		HttpSession sessao = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		login = (LoginBean) sessao.getAttribute(WorkflowRGC.LOGIN_USUARIO);
		
		//implementar limpar, carregar listas e buscar
		this.buscar();
	}

	//---------getters e setters---------
	public ItemBean getItem() {
		return item;
	}

	public void setItem(ItemBean item) {
		if (item != null){
			this.item = item;
		}else{
			this.item = new ItemBean();
		}
	}
	
	public List<ItemBean> getItens() {
		return itens;
	}

	public void setItens(List<ItemBean> itens) {
		this.itens = itens;
	}

	public ItemBean getFiltro() {
		return filtro;
	}

	public void setFiltro(ItemBean filtro) {
		this.filtro = filtro;
	}

	public List<ClienteBean> getClientes() {
		return clientes;
	}

	public void setClientes(List<ClienteBean> clientes) {
		this.clientes = clientes;
	}

	public List<DepartamentoBean> getDepartamentos() {
		return departamentos;
	}

	public void setDepartamentos(List<DepartamentoBean> departamentos) {
		this.departamentos = departamentos;
	}

	public List<NCMBean> getNcms() {
		return ncms;
	}

	public void setNcms(List<NCMBean> ncms) {
		this.ncms = ncms;
	}
	//---------fim getters e setters---------
	
	//---------metodos de carregamento---------
	public void buscar(){
		// implementar parametros de busca
		this.itens = new ArrayList<ItemBean>();
		this.itens.add(new ItemBean(new Long(1), new ClienteBean(new Long(1), "Volvo", "32165498732145", "www.volvo.com.br"), new DepartamentoBean(new Long(1), "Peças"), new NCMBean(), "Bosch", "12354A", "kjha", "6548", "Parafuso galvanizado", new Date(new java.util.Date().getTime()), new Date(new java.util.Date().getTime()), "S", new ArrayList<ClassificacaoBean>(), new ArrayList<ItemCatalogoBean>()));
		this.itens.add(new ItemBean(new Long(1), new ClienteBean(new Long(1), "Volvo", "32165498732145", "www.volvo.com.br"), new DepartamentoBean(new Long(1), "Peças"), new NCMBean(), "Bosch", "987AJ", "lkq55", "654", "Porca", new Date(new java.util.Date().getTime()), new Date(new java.util.Date().getTime()), "S", new ArrayList<ClassificacaoBean>(), new ArrayList<ItemCatalogoBean>()));
		this.itens.add(new ItemBean(new Long(1), new ClienteBean(new Long(1), "Volvo", "32165498732145", "www.volvo.com.br"), new DepartamentoBean(new Long(1), "Peças"), new NCMBean(), "Bosch", "6548A", "as54", "345", "Prego", new Date(new java.util.Date().getTime()), new Date(new java.util.Date().getTime()), "S", new ArrayList<ClassificacaoBean>(), new ArrayList<ItemCatalogoBean>()));
	}
	
	public void limpar(){
		filtro = new ItemBean();
	}
	//--------- fim dos metodos de carregamento---------
	
	//---------metodos de ações---------	
	/**
	 * Método que realiza a inclusão do item no sistema
	 */
	public void incluir(){
		RequestContext request = RequestContext.getCurrentInstance();
		
		if (validaIncluir()){
			
			this.item.setDataCadastro(new Date(new java.util.Date().getTime()));
			this.item.setStatus("P");
			repository.save(this.item);
			
			//if (){
			
				MessagesView.info("",resource.getString("label_msg_novo_item", login.getLocale()));
				request.addCallbackParam("sucesso", true);
			    //limpa atributo
			    this.item = new ItemBean();
			    
		    /*}else{
		        MessagesView.error("",resource.getString("label_msg_erro_novo_item", login.getLocale()));
    			Utilitarios.rollback(login.getConexao());
		    	request.addCallbackParam("sucesso", false);		    		
	    	}*/
			
		}else{
			request.addCallbackParam("sucesso", false);
		}
	}
	
	private boolean validaIncluir(){		
		boolean retorno = true;
		
		if (this.item.getCliente() == null){
		    MessagesView.info("AVISO",resource.getString("label_msg_cliente", login.getLocale()));
		    retorno = false;
		}
					
		if (this.item.getDepartamento() == null){
		    MessagesView.info("AVISO",resource.getString("label_msg_departamento", login.getLocale()));
		    retorno = false;
		}
					
		if ("".equals(this.item.getPartNumber().trim())){
		    MessagesView.info("AVISO",resource.getString("label_msg_part_number", login.getLocale()));
		    retorno = false;
		}
					
		if ("".equals(this.item.getDescricao().trim())){
		    MessagesView.info("AVISO",resource.getString("label_msg_descricao", login.getLocale()));
		    retorno = false;
		}
					
		return retorno;
	}
	
	/**
	 * Método que edita o item
	 */
	public void editar(){
		RequestContext request = RequestContext.getCurrentInstance();
		
		repository.save(this.item);
		
		//if (){
			MessagesView.info("",resource.getString("label_msg_editar_item", login.getLocale()));
			request.addCallbackParam("sucesso", true);
			
			//limpa atributo
			this.item = new ItemBean();    	
		/*}else{
			MessagesView.error("",resource.getString("label_msg_erro_editar_item", login.getLocale()));  
			request.addCallbackParam("sucesso", false);
		}*/
	}

	/**
	 * Método que exclui o item
	 */
	public void excluir(){
		RequestContext request = RequestContext.getCurrentInstance();
		
		repository.delete(this.item.getId());
		ItemBean item = repository.findOne(this.item.getId());
		
		if (item == null){
		    MessagesView.info("",resource.getString("label_msg_excluir_item", login.getLocale()));
		    request.addCallbackParam("sucesso", true);
			
		    //limpa atributo
		    this.item = new ItemBean();   		
		}else{
		    MessagesView.error("",resource.getString("label_msg_erro_excluir_item", login.getLocale()));	
		    request.addCallbackParam("sucesso", false);
		}
	}
	//---------fim dos metodos de ações---------

	/**
	 * Método que carrega o combo de clientes
	 */
	public void carregarClientes(){
		//this.clientes = (List<ClienteBean>) clienteRepository.findAll();
		
		this.clientes.add(new ClienteBean(new Long(1), "Volvo", "32165498732145", "www.volvo.com.br"));
	}

	/**
	 * Método que carrega o combo de departamentos
	 */
	public void carregarDepartamentos(){
		//this.departamentos = (List<DepartamentoBean>) departamentoRepository.findAll();
		
		this.departamentos.add(new DepartamentoBean(new Long(1), "Peças"));
		this.departamentos.add(new DepartamentoBean(new Long(2), "Compras"));
		this.departamentos.add(new DepartamentoBean(new Long(3), "Produção"));
	}

	/**
	 * Método que carrega o combo de ncms
	 */
	public void carregarNcms(){
		this.ncms = (List<NCMBean>) ncmRepository.findAll();
	}

}
