package br.eng.rgc.managedbeans;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import br.eng.rgc.beans.ClienteBean;
import br.eng.rgc.beans.DepartamentoBean;
import br.eng.rgc.beans.ItemBean;
import br.eng.rgc.beans.NCMBean;

/***
 * @author Matheus R. Torres
 * @version 1.0
 * @Date 06/2015
 *
 * ManagedBean para controlar o cadastro de DREs
 */
@ManagedBean
@ViewScoped
public class TesteMB implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7732470655740970798L;

	//private UlResourceBundle resource = new UlResourceBundle("teste");
	private List<ItemBean> itens;
	private ItemBean item;
	private double valor;
	/*private DREService services;
	private LoginBean login;
	private DREFiltro filtro;

	// variáveis utilizadas para selecionar Pessoa
	private List<PessoaBean> pessoas;
	private PessoaService pessoaService;

	// variáveis utilizadas para selecionar Plano de Conta
	private List<PlanoContaBean> planosConta;
	private PlanoContaService planoContaService;
	private PlanoContaFiltro planoContaFiltro;

	// variáveis utilizadas para inserir vínculo de DRE e Plano de Conta
	private List<DREContaBean> dresConta;
	private DREContaBean dreConta;
	private DREContaService dreContaService;
	
	private List<Integer> listaOrdens;
	private int ordemMinima;
	private int ordemAnterior;*/
	
	public TesteMB(){}
	
	@PostConstruct
	public void init(){
		//recuperando o usuário na sessao
		HttpSession sessao = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		valor = 1.2;
		/*login = (LoginBean) sessao.getAttribute(ControleCusto.LOGIN_USUARIO);		
		services = new DREService(login.getConexao());	
		filtro = new DREFiltro();			
		pessoaService = new PessoaService(login.getConexao());	
		dreContaService = new DREContaService(login.getConexao());	
		planoContaService = new PlanoContaService(login.getConexao());
		planoContaFiltro = new PlanoContaFiltro();

		listaOrdens = new ArrayList<Integer>();

		limpar();
		carregaPessoas();
		filtro.setListaPessoas(pessoas);
        filtro.setPessoa(login.getPessoa());*/
        buscar();
	}

	//---------Inicio Getters e Setters-----------
	public List<ItemBean> getItens() {
		return itens;
	}

	public ItemBean getItem() {
		if(item == null){
			this.item = new ItemBean();
		}
		
		return item;
	}

	public double getValor() {		
		return valor;
	}

	public void setItem(ItemBean item) {
		if(item != null){
			this.item = item;
		}else{
			this.item = new ItemBean();
		}
	}

	//--------Fim Getters e Setters-----------


	//----Metodos de carregamento -----	
	public void buscar(){
		this.itens = new ArrayList<ItemBean>();
		this.itens.add(new ItemBean(new Long(0), new ClienteBean(), new DepartamentoBean(), new NCMBean(), "TRW", "FE1245", "", "","Pastilha de Freio", new Date(new java.util.Date().getTime()), new Date(new java.util.Date().getTime()),"A", null, null));
		this.itens.add(new ItemBean(new Long(1), new ClienteBean(), new DepartamentoBean(), new NCMBean(), "TRW", "JHU584", "", "","Disco de Freio", new Date(new java.util.Date().getTime()), new Date(new java.util.Date().getTime()),"A", null, null));
		this.itens.add(new ItemBean(new Long(2), new ClienteBean(), new DepartamentoBean(), new NCMBean(), "DS", "6548GF", "", "","Diafragma Monoponto", new Date(new java.util.Date().getTime()), new Date(new java.util.Date().getTime()),"A", null, null));
		this.itens.add(new ItemBean(new Long(3), new ClienteBean(), new DepartamentoBean(), new NCMBean(), "Perfect Brasil", "LKQUI654", "", "","Terminal", new Date(new java.util.Date().getTime()), new Date(new java.util.Date().getTime()),"A", null, null));
		this.itens.add(new ItemBean(new Long(4), new ClienteBean(), new DepartamentoBean(), new NCMBean(), "TRW", "LKJ4AS", "", "","Barra Estabilizadora", new Date(new java.util.Date().getTime()), new Date(new java.util.Date().getTime()),"A", null, null));
		this.itens.add(new ItemBean(new Long(5), new ClienteBean(), new DepartamentoBean(), new NCMBean(), "DS", "687LKJA", "", "","Sensor de Posição da Borboleta", new Date(new java.util.Date().getTime()), new Date(new java.util.Date().getTime()),"A", null, null));
		this.itens.add(new ItemBean(new Long(6), new ClienteBean(), new DepartamentoBean(), new NCMBean(), "Autobrás", "JDS761", "", "","Reparo do Braço Oscilante", new Date(new java.util.Date().getTime()), new Date(new java.util.Date().getTime()),"A", null, null));
		this.itens.add(new ItemBean(new Long(7), new ClienteBean(), new DepartamentoBean(), new NCMBean(), "Autobrás", "AILEWK58", "", "","Ponteira Cabo Cromado do Câmbio", new Date(new java.util.Date().getTime()), new Date(new java.util.Date().getTime()),"A", null, null));
		this.itens.add(new ItemBean(new Long(8), new ClienteBean(), new DepartamentoBean(), new NCMBean(), "DS", "98LKHU", "", "","Sensor de Detonação", new Date(new java.util.Date().getTime()), new Date(new java.util.Date().getTime()),"A", null, null));
		this.itens.add(new ItemBean(new Long(9), new ClienteBean(), new DepartamentoBean(), new NCMBean(), "Perfect Brasil", "QGHCDA85G", "", "","Bucha", new Date(new java.util.Date().getTime()), new Date(new java.util.Date().getTime()),"A", null, null));
		this.itens.add(new ItemBean(new Long(10), new ClienteBean(), new DepartamentoBean(), new NCMBean(), "Autobrás", "934HJ651", "", "","Caixa Rótula da Alavanca", new Date(new java.util.Date().getTime()), new Date(new java.util.Date().getTime()),"A", null, null));
		this.itens.add(new ItemBean(new Long(11), new ClienteBean(), new DepartamentoBean(), new NCMBean(), "Perfect Brasil", "LKJHA65TRC", "", "","Bomba D'água", new Date(new java.util.Date().getTime()), new Date(new java.util.Date().getTime()),"A", null, null));
		this.itens.add(new ItemBean(new Long(12), new ClienteBean(), new DepartamentoBean(), new NCMBean(), "DS", "934HJ651", "", "","Atuador da Marcha Lenta", new Date(new java.util.Date().getTime()), new Date(new java.util.Date().getTime()),"A", null, null));
	}
	
	//----Metodos de carregamento -----	
	public void incluir(){
	}
}