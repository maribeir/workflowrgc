package br.eng.rgc.managedbeans;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;

import br.eng.rgc.beans.CargoBean;
import br.eng.rgc.beans.ClassificacaoBean;
import br.eng.rgc.beans.IndispUsuarioBean;
import br.eng.rgc.beans.LogBean;
import br.eng.rgc.beans.LoginBean;
import br.eng.rgc.beans.UsuarioBean;
import br.eng.rgc.beans.UsuarioClienteBean;
import br.eng.rgc.beans.UsuarioDepartamentoBean;
import br.eng.rgc.repositories.CargoRepository;
import br.eng.rgc.repositories.UsuarioRepository;
import br.eng.rgc.util.MD5Util;
import br.eng.rgc.util.MessagesView;
import br.eng.rgc.util.UlResourceBundle;
import br.eng.rgc.util.WorkflowRGC;

/***
 * @author Matheus R. Torres
 * @version 1.0
 * @Date 08/2017
 *
 * ManagedBean para controlar o Cadastro de Usuário.
 */
@ManagedBean
@ViewScoped
public class UsuarioMB implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4656333028738476122L;
	
	@Autowired
	private UsuarioRepository repository;
	
	@Autowired
	private CargoRepository cargoRepository;	
	
	private UlResourceBundle resource = new UlResourceBundle("usuario");
	private List<CargoBean> cargos;
	private List<UsuarioBean> usuarios;
	private UsuarioBean usuario;
	private UsuarioBean filtro;
	private LoginBean login;
	
	public UsuarioMB(){}
	
	@PostConstruct
	public void init(){
		//recuperando o usuário na sessao
		HttpSession sessao = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		login = (LoginBean) sessao.getAttribute(WorkflowRGC.LOGIN_USUARIO);

		limpar();
		carregarCargos();
		buscar();
	}

	//---------Inicio Getters e Setters-----------
	public List<UsuarioBean> getUsuarios() {
		return usuarios;
	}

	public UsuarioBean getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioBean usuario) {
		if(usuario != null){
			this.usuario = usuario;
		}else{
			this.usuario = new UsuarioBean();
		}
	}
	
	public UsuarioBean getFiltro() {
		return filtro;
	}

	public void setFiltro(UsuarioBean filtro) {
		this.filtro = filtro;
	}
	
	public List<CargoBean> getCargos() {
		return cargos;
	}

	public void setCargos(List<CargoBean> cargos) {
		this.cargos = cargos;
	}

	//--------Fim Getters e Setters-----------


	//----Metodos de carregamento -----	
	public void buscar(){		
		/*
		if (!"".equals(filtro.getNome()))
			if (!"".equals(filtro.getDisponivel()))
				// nome + disponivel + ativo
				if (!"".equals(filtro.getAtivo()))
					this.usuarios = (List<UsuarioBean>) repository.findByNomeAndDisponivelAndAtivo(filtro.getNome(), filtro.getDisponivel(), filtro.getAtivo());
		        // nome + disponivel
				else
					this.usuarios = (List<UsuarioBean>) repository.findByNomeAndDisponivel(filtro.getNome(), filtro.getDisponivel());
			else
				// nome + ativo
				if (!"".equals(filtro.getAtivo()))
				    this.usuarios = (List<UsuarioBean>) repository.findByNomeAndAtivo(filtro.getNome(), filtro.getAtivo());
		        // nome
				else
		            this.usuarios = (List<UsuarioBean>) repository.findByNome(filtro.getNome());
		else if (!"".equals(filtro.getDisponivel()))
	        // disponivel + ativo
			if (!"".equals(filtro.getAtivo()))
				this.usuarios = (List<UsuarioBean>) repository.findByDisponivelAndAtivo(filtro.getDisponivel(), filtro.getAtivo());
            // disponivel
			else
				this.usuarios = (List<UsuarioBean>) repository.findByDisponivel(filtro.getDisponivel());
        // ativo
		else if (!"".equals(filtro.getAtivo()))
			this.usuarios = (List<UsuarioBean>) repository.findByAtivo(filtro.getAtivo());
        // sem filtro
		else
		    this.usuarios = (List<UsuarioBean>) repository.findAll();
		*/
		
		this.usuarios = new ArrayList<UsuarioBean>();
		this.usuarios.add(new UsuarioBean(new Long(1), new CargoBean(new Long(1), "Gerente"), "Fábio", "32165498702", "fabio@rgc.com.br", "fabio", "3232-3232", "teste", "S", "S", new Date(new java.util.Date().getTime()), new ArrayList<ClassificacaoBean>(), new ArrayList<IndispUsuarioBean>(), new ArrayList<IndispUsuarioBean>(), new ArrayList<IndispUsuarioBean>(), new ArrayList<LogBean>(), new ArrayList<UsuarioClienteBean>(), new ArrayList<UsuarioDepartamentoBean>()));
		this.usuarios.add(new UsuarioBean(new Long(2), new CargoBean(new Long(1), "Gerente"), "Matheus", "32165498702", "matheus@matheus.com", "matheus", "3232-4040", "teste", "S", "S", new Date(new java.util.Date().getTime()), new ArrayList<ClassificacaoBean>(), new ArrayList<IndispUsuarioBean>(), new ArrayList<IndispUsuarioBean>(), new ArrayList<IndispUsuarioBean>(), new ArrayList<LogBean>(), new ArrayList<UsuarioClienteBean>(), new ArrayList<UsuarioDepartamentoBean>()));
		this.usuarios.add(new UsuarioBean(new Long(3), new CargoBean(new Long(1), "Gerente"), "Fernando", "32165498702", "fernando@fernando.com", "fernando", "3232-9999", "teste", "S", "S", new Date(new java.util.Date().getTime()), new ArrayList<ClassificacaoBean>(), new ArrayList<IndispUsuarioBean>(), new ArrayList<IndispUsuarioBean>(), new ArrayList<IndispUsuarioBean>(), new ArrayList<LogBean>(), new ArrayList<UsuarioClienteBean>(), new ArrayList<UsuarioDepartamentoBean>()));
	}
	
	public void limpar(){
		//this.filtro = new UsuarioFiltro();
		filtro = new UsuarioBean();
	}
	//----Fim dos Metodos de carregamento -----
	
	//-----Métodos de acoes ---//
	
	/**
	 * Método que realiza a inclusão do usuario no sistema
	 */
	public void incluir(){
		RequestContext request = RequestContext.getCurrentInstance();
		
		if (validaIncluir()){
			
			this.usuario.setDataCadastro(new Date(new java.util.Date().getTime()));
			this.usuario.setAtivo("S");
			this.usuario.setSenha(MD5Util.getEncrypt(this.usuario.getSenha()));
			repository.save(this.usuario);
			
			//if (){
		    	
		    	/*UsuarioEmpresaBean usuarioEmpresa = new UsuarioEmpresaBean();
		    	usuarioEmpresa.setUsuario(this.usuario);
		    	usuarioEmpresa.setEmpresa(this.empresa);
		    	
		    	if (usuarioEmpresaService.incluir(usuarioEmpresa)){
		    		*/
				    MessagesView.info("",resource.getString("label_msg_novo_usuario", login.getLocale()));
				    request.addCallbackParam("sucesso", true);
			
				    //limpa atributo
				    this.usuario = new UsuarioBean();
				    
		    	/*}else{
			        MessagesView.error("",resource.getString("label_msg_erro_novo_usuario", login.getLocale()));
	    			Utilitarios.rollback(login.getConexao());
			    	request.addCallbackParam("sucesso", false);		    		
		    	}*/
		    	
		    /*}else{
		        MessagesView.error("",resource.getString("label_msg_erro_novo_usuario", login.getLocale()));
		    	request.addCallbackParam("sucesso", false);
		    }*/
		    
		}else{
	    	request.addCallbackParam("sucesso", false);
		}
	}
	
	/**
	 * Método que realiza a validação dos dados para inclusão do usuário no sistema
	 */
	private boolean validaIncluir(){		
		boolean retorno = true;
		
		/*if ("".equals(this.usuario.getNome().trim())){
		    MessagesView.info("AVISO",resource.getString("label_msg_nome", login.getLocale()));
		    retorno = false;
		}

		if ("".equals(this.usuario.getUsername().trim())){
		    MessagesView.info("AVISO",resource.getString("label_msg_username", login.getLocale()));
		    retorno = false;
		}

		if ("".equals(this.usuario.getSenha().trim())){
		    MessagesView.info("AVISO",resource.getString("label_msg_senha", login.getLocale()));
		    retorno = false;
		}

		if ("".equals(this.usuario.getEmail().trim())){
		    MessagesView.info("AVISO",resource.getString("label_msg_email", login.getLocale()));
		    retorno = false;
		}

		if ("".equals(this.usuario.getTelefone().trim())){
		    MessagesView.info("AVISO",resource.getString("label_msg_telefone", login.getLocale()));
		    retorno = false;
		}

		if (this.empresa.getId() == 0){
		    MessagesView.info("AVISO",resource.getString("label_msg_empresa", login.getLocale()));
		    retorno = false;
		}*/
		
		return retorno;
	}
	
	/**
	 * Método que edita o usuario
	 */
	public void editar(){
		RequestContext request = RequestContext.getCurrentInstance();
		
		repository.save(this.usuario);
		
		//if (){
			MessagesView.info("",resource.getString("label_msg_editar_usuario", login.getLocale()));
			request.addCallbackParam("sucesso", true);
			
			//limpa atributo
			this.usuario = new UsuarioBean();    	
		/*}else{
			MessagesView.error("",resource.getString("label_msg_erro_editar_usuario", login.getLocale()));  
			request.addCallbackParam("sucesso", false);
		}*/
	}
	
	
	/**
	 * Método que exclui o usuario
	 */
	public void excluir(){
		RequestContext request = RequestContext.getCurrentInstance();
		
		repository.delete(this.usuario.getId());
		UsuarioBean usuario = repository.findOne(this.usuario.getId());
		
		if (usuario == null){
		    MessagesView.info("",resource.getString("label_msg_excluir_usuario", login.getLocale()));
		    request.addCallbackParam("sucesso", true);
			
		    //limpa atributo
		    this.usuario = new UsuarioBean();   		
		}else{
		    MessagesView.error("",resource.getString("label_msg_erro_excluir_usuario", login.getLocale()));	
		    request.addCallbackParam("sucesso", false);
		}
	}

	
	//-----Fim dos Métodos de acoes ---//
	
	/**
	 * Método que carrega o combo de Cargos
	 */
	public void carregarCargos(){
		//this.cargos = (List<CargoBean>) cargoRepository.findAll();
		this.cargos = new ArrayList<CargoBean>();
		this.cargos.add(new CargoBean(new Long(1), "Gerente"));
		this.cargos.add(new CargoBean(new Long(2), "Cargo 2"));
	}
}