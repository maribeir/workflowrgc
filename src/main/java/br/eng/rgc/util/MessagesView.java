package br.eng.rgc.util;
/***
 * @author Matheus R. Torres
 * @version 1.0 
 * @Date 09/2015
 * 
 * Classe para exibir as menssagens na tela para o usuário
 */

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class MessagesView {
	
	
	/**
	 * Método que exibe uma mensagem de informação
	 * @param title
	 * @param message
	 */
	public static void info(String title,String message) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, title, message));
    }

	
	/**
	 * Método que exibe uma mensagem de Alerta
	 * @param title
	 * @param message
	 */
    public static void warn(String title,String message) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, title, message));
    }
     
    
	/**
	 * Método que exibe uma mensagem de Erro
	 * @param title
	 * @param message
	 */
    public static void error(String title,String message) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, title, message));
    }
     
	/**
	 * Método que exibe uma mensagem de Erro Fatal
	 * @param title
	 * @param message
	 */
    public static void fatal(String title,String message) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, title, message));
    }
}
