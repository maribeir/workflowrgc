package br.eng.rgc.util;

public class WorkflowRGC {
	
	public static final String LOGIN_SUCESSO  = "home.xhtml";
	public static final String LOGIN_SEL_PESSOA  =  "selecionar_pessoa.xhtml";
	public static final String LOGIN_EXPIRADO = "troca_senha.workflowrgc";
	public static final String LOGIN_USUARIO  = "loginUsuario";
	public static final String SIM = "S";
	public static final String NAO = "N";
	public static final String ATIVO = "A";
	public static final String FINALIZADO = "F";
	public static final String CANCELADO = "C";
	
}
