package br.eng.rgc.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Util {
	 
	 public static final String getEncrypt(String key){
	  
		 MessageDigest en = null;
	  
		 try {
			 en = MessageDigest.getInstance("MD5");
		 } catch (NoSuchAlgorithmException e) {   
			 e.printStackTrace();
		 }
	  
		 en.update(key.getBytes(),0,key.length());
		 
		 return String.valueOf(new BigInteger(1, en.digest()).toString(16)).toUpperCase();
	 }
}