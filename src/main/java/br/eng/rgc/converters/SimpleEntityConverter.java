package br.eng.rgc.converters;

import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.eng.rgc.beans.BaseBean;



@FacesConverter(value="simpleEntityConverter")
public class SimpleEntityConverter implements Converter 
{  
	
    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {  
        if (value != null) {  
            return this.getAttributesFrom(component).get(value);  
        }  
        return null;  
        
    }  
  
    public String getAsString(FacesContext ctx, UIComponent component, Object value) {  
  
        if (value != null  
                && !"".equals(value)) {  
  
        	BaseBean entity = (BaseBean) value;  
  
            // adiciona item como atributo do componente  
            this.addAttribute(component, entity);  
  
            Long codigo = entity.getId();  
            if (codigo != null) {  
                return String.valueOf(codigo);  
            }  
        }
        else
        {
        	//value = (String) "";
        }
  
        return (String) value.toString();  
    }  
  
    protected void addAttribute(UIComponent component, BaseEntity o) {  
    	/*System.out.println("");
    	System.out.println("");
    	System.out.println(" --ADD ATTRIBUTE --");
    	System.out.println(" :BaseEntity: " + o.getClass().getName() + " || ID: " + o.getId());
    	System.out.println("");  
    	System.out.println("");*/
    	if (o != null && o.getId() != null){
            String key = o.getId().toString();
            this.getAttributesFrom(component).put(key, o);  
    	}
    }  
  
    protected Map<String, Object> getAttributesFrom(UIComponent component) {  
        return component.getAttributes();  
    }  
  
}  